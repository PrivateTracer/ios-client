//
//  TracerTokenGeneratorTest.swift
//  PrivateTracerTests
//
//  Created by Dirk Groten on 25/04/2020.
//

import XCTest
@testable import PrivateTracer

class TracerTokenGeneratorTest: XCTestCase {
    
    func testService() throws {
        let service: TracerTokenGenerator = TracerTokenGeneratorImplementation()
        
        for _ in (0..<1000) {
            let token = service.newTracerToken()
            XCTAssert(service.isValidTracerToken(token))
        }
        
        let correctToken = "964-F3A-GKX-3"
        let wrongToken = "964-F3A-GKX-2"
        XCTAssert(service.isValidTracerToken(correctToken))
        XCTAssertFalse(service.isValidTracerToken(wrongToken))
        
    }
}

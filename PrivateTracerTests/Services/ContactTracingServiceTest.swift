//
//  ContactTracingServiceTest.swift
//  PrivateTracerTests
//
//  Created by Dirk Groten on 26/04/2020.
//

import CoreBluetooth
import XCTest
@testable import PrivateTracer

class ContactTracingServiceImplementationTest: XCTestCase {
    private let manager: ObservationManager = ContactTracingServiceImplementation()
    private let contacts = ServiceInjection.contactRecordingService
    private var peripheral: CBPeripheral?
    private var uuid: UUID?
    
    override func setUp() {
        uuid = UUID()
    }
    
    func testFirstObservationWithoutEphId() throws {
        manager.addObservationWithUuid(uuid!, values: Observation(ephId: nil, timestamp: Date(), txLevel: 5.0, rssi: -55.0))
        XCTAssertFalse(manager.hasObservationWithUuid(uuid!))
        let service = manager as! ContactTracingServiceImplementation
        XCTAssertEqual(service.cachedObservations.count, 1)
    }
    
    func testSecondObservationWithEphIDGetsMerged() throws {
        let ephID = UUID()
        let date = Date()
        manager.addObservationWithUuid(uuid!, values: Observation(ephId: nil, timestamp: date, txLevel: 5.0, rssi: -55.0))
        manager.addObservationWithUuid(uuid!, values: Observation(ephId: ephID, timestamp: date + TimeInterval(30), txLevel: nil, rssi: nil))
        let service = manager as! ContactTracingServiceImplementation
        XCTAssertTrue(service.hasObservationWithUuid(uuid!))
        XCTAssertEqual(1, service.observations[uuid!]!.count)
        XCTAssertEqual(0, service.cachedObservations.count)
        let observation = service.observations[uuid!]!.first
        XCTAssertEqual(date + TimeInterval(30), observation?.timestamp)
    }
    
    func testSecondObservationWithoutEphIDGetsRecorded() throws {
        let ephID = UUID()
        let date = Date()
        manager.addObservationWithUuid(uuid!, values: Observation(ephId: ephID, timestamp: date, txLevel: 5.0, rssi: -55.0))
        manager.addObservationWithUuid(uuid!, values: Observation(ephId: nil, timestamp: date + TimeInterval(10), txLevel: 5.0, rssi: -63.0))
        let service = manager as! ContactTracingServiceImplementation
        XCTAssertEqual(2, service.observations[uuid!]!.count)
        let lastObservation = service.observations[uuid!]!.last!
        XCTAssertEqual(ephID, lastObservation.ephId)
        XCTAssertEqual(date + TimeInterval(10), lastObservation.timestamp)
    }
    
    func testSecondObservationWithEphIDGetsRecorded() throws {
        let ephID = UUID()
        let date = Date()
        manager.addObservationWithUuid(uuid!, values: Observation(ephId: ephID, timestamp: date, txLevel: 5.0, rssi: -55.0))
        manager.addObservationWithUuid(uuid!, values: Observation(ephId: ephID, timestamp: date + TimeInterval(10), txLevel: 5.0, rssi: -63.0))
        let service = manager as! ContactTracingServiceImplementation
        XCTAssertEqual(2, service.observations[uuid!]!.count)
        let lastObservation = service.observations[uuid!]!.last!
        XCTAssertEqual(ephID, lastObservation.ephId)
        XCTAssertEqual(date + TimeInterval(10), lastObservation.timestamp)
    }
    
    func testSecondObservationWithDifferenUuidSameEphIDRecorded() throws {
        let ephID = UUID()
        let date = Date()
        let uuid2 = UUID()
        manager.addObservationWithUuid(uuid!, values: Observation(ephId: ephID, timestamp: date, txLevel: 5.0, rssi: -64.0))
        manager.addObservationWithUuid(uuid2, values: Observation(ephId: ephID, timestamp: date + TimeInterval(30), txLevel: 5.0, rssi: -72.0))
        let service = manager as! ContactTracingServiceImplementation
        XCTAssertEqual(nil, service.observations[uuid!]?.count)
        XCTAssertEqual(2, service.observations[uuid2]!.count)
        let lastObservation = service.observations[uuid2]!.last!
        XCTAssertEqual(ephID, lastObservation.ephId)
        XCTAssertEqual(date + TimeInterval(30), lastObservation.timestamp)
    }
    
    func testSecondObservationForDifferentDeviceRecorded() throws {
        let ephID = UUID()
        let ephID2 = UUID()
        let date = Date()
        let uuid2 = UUID()
        manager.addObservationWithUuid(uuid!, values: Observation(ephId: ephID, timestamp: date, txLevel: 5.0, rssi: -64.0))
        manager.addObservationWithUuid(uuid2, values: Observation(ephId: ephID2, timestamp: date + TimeInterval(30), txLevel: 5.0, rssi: -72.0))
        let service = manager as! ContactTracingServiceImplementation
        XCTAssertEqual(1, service.observations[uuid!]?.count)
        XCTAssertEqual(1, service.observations[uuid2]?.count)
        let lastObservation = service.observations[uuid2]!.last!
        XCTAssertEqual(ephID2, lastObservation.ephId)
        XCTAssertEqual(date + TimeInterval(30), lastObservation.timestamp)
    }
    
    func testFlushMultipleObservations() throws {
        let ephID = UUID()
        let ephID2 = UUID()
        let date = Date()
        let uuid2 = UUID()
        let whatWeExpect = expectation(description: "Remove Core Data")

        manager.addObservationWithUuid(uuid!, values: Observation(ephId: ephID, timestamp: date, txLevel: 5.0, rssi: -64.0))
        manager.addObservationWithUuid(uuid!, values: Observation(ephId: ephID, timestamp: date + TimeInterval(30), txLevel: 5.0, rssi: -62))
        manager.addObservationWithUuid(uuid2, values: Observation(ephId: ephID2, timestamp: date + TimeInterval(1), txLevel: 5.0, rssi: -72.0))
        manager.addObservationWithUuid(uuid2, values: Observation(ephId: ephID2, timestamp: date + TimeInterval(31), txLevel: 5.0, rssi: -64))
        let service = manager as! ContactTracingServiceImplementation
        contacts.removeAllData { _ in
            service.stop()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.contacts.recordedContacts {
                    result in
                    switch result {
                    case .success(let recordedContacts):
                        XCTAssertEqual(2, recordedContacts.count)
                    case .failure(let error):
                        XCTFail("Error: \(error)")
                    }
                    whatWeExpect.fulfill()
                }
            }
        }
        waitForExpectations(timeout: TimeInterval(5)) { err in
            if let error = err {
                XCTFail("Error: \(error)")
            }
        }
    }
    
    func testFlushMultipleObservationsWithLowRSSI() throws {
        let ephID = UUID()
        let ephID2 = UUID()
        let date = Date()
        let uuid2 = UUID()
        let whatWeExpect = expectation(description: "Remove Core Data")
        
        let threshold = PrivateTracerConfig.RiskyContactMinimumRssi
        manager.addObservationWithUuid(uuid!, values: Observation(ephId: ephID, timestamp: date, txLevel: 5.0, rssi: -64.0))
        manager.addObservationWithUuid(uuid!, values: Observation(ephId: ephID, timestamp: date + TimeInterval(30), txLevel: 5.0, rssi: -62))
        manager.addObservationWithUuid(uuid2, values: Observation(ephId: ephID2, timestamp: date + TimeInterval(1), txLevel: 5.0, rssi: threshold - 2.0 as NSNumber))
        manager.addObservationWithUuid(uuid2, values: Observation(ephId: ephID2, timestamp: date + TimeInterval(31), txLevel: 5.0, rssi: threshold - 3.0 as NSNumber))
        let service = manager as! ContactTracingServiceImplementation
        contacts.removeAllData { _ in
            service.stop()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.contacts.recordedContacts {
                    result in
                    switch result {
                    case .success(let recordedContacts):
                        XCTAssertEqual(1, recordedContacts.count)
                    case .failure(let error):
                        XCTFail("Error: \(error)")
                    }
                    whatWeExpect.fulfill()
                }
            }
        }
        
        waitForExpectations(timeout: TimeInterval(5)) { err in
            if let error = err {
                XCTFail("Error: \(error)")
            }
        }
    }
}

//
//  DP3TServiceImplementationTest.swift
//  PrivateTracerTests
//
//  Created by Stijn Spijker.
//

import XCTest
@testable import PrivateTracer

class DP3TServiceImplementationTest: XCTestCase {

    override func setUp() {
        // Always use a fresh UserDefaults
        if let bundleID = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: bundleID)
        }
    }
    
    func testService() throws {
        
        let service: DP3TService = DP3TServiceImplementation()
        
        XCTAssertNil(service.epochAndSeed)
        
        let expectation = XCTestExpectation(description: "Callback is immediatly called")
        service.start { newEphemeralId in
            
            XCTAssertNotNil(service.epochAndSeed)
            
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5.0)
        
    }
    
    func testSaving() throws {
        let seedService = SeedRecordsServiceImplementation()
        let expectation1 = XCTestExpectation(description: "Seed history is deleted")
        
        // Delete all data first to reset to 0
        seedService.removeAllData { _ in
            seedService.seedHistory { result in
                let seeds = try? result.get()
                XCTAssertNotNil(seeds)
                XCTAssertEqual(seeds!.count, 0)
                expectation1.fulfill()
            }
        }
        wait(for: [expectation1], timeout: 5.0)
        
        // Create a first seed
        let service = DP3TServiceImplementation()
        let expectation = XCTestExpectation(description: "Callback is immediately called")
        var seed: DP3TEpochAndSeed?
        service.start { _ in
            seed = service.epochAndSeed!
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
        XCTAssert(seed != nil)

        // Check that it's saved
        let expectation2 = XCTestExpectation(description: "CoreData returns data")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            seedService.seedHistory {result in
                let records = try? result.get()
                XCTAssert(records != nil)
                XCTAssertEqual(records!.count, 1)
                XCTAssertEqual(records!.first!.seed, seed!.seed.base64EncodedString())
                XCTAssertEqual(records!.first!.epochId, seed!.id)
                expectation2.fulfill()
            }

        }
        wait(for: [expectation2], timeout: 5.0)
    }
}

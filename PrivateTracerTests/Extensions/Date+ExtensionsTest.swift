//
//  Date+ExtensionsTest.swift
//  PrivateTracerTests
//
//  Created by Stijn Spijker.
//

import XCTest
@testable import PrivateTracer

class DateExtensionsTests: XCTestCase {
    
    func test_isBefore() {
        XCTAssert(Date(timeIntervalSince1970: 10).isBefore(Date(timeIntervalSince1970: 20)))
        XCTAssert(Date(timeIntervalSince1970: 1337).isBefore(Date(timeIntervalSince1970: 1338)))
        XCTAssert(Date(timeIntervalSince1970: 0).isBefore(Date(timeIntervalSince1970: 1)))
        XCTAssert(Date(timeIntervalSince1970: 123456789).isBefore(Date(timeIntervalSince1970: 987654321)))
        
        XCTAssertFalse(Date(timeIntervalSince1970: 20).isBefore(Date(timeIntervalSince1970: 10)))
        XCTAssertFalse(Date(timeIntervalSince1970: 1338).isBefore(Date(timeIntervalSince1970: 1337)))
        XCTAssertFalse(Date(timeIntervalSince1970: 1).isBefore(Date(timeIntervalSince1970: 0)))
        XCTAssertFalse(Date(timeIntervalSince1970: 987654321).isBefore(Date(timeIntervalSince1970: 123456789)))
        
        XCTAssertFalse(Date(timeIntervalSince1970: 0).isBefore(Date(timeIntervalSince1970: 0)))
        XCTAssertFalse(Date(timeIntervalSince1970: 1).isBefore(Date(timeIntervalSince1970: 1)))
        XCTAssertFalse(Date(timeIntervalSince1970: 1337).isBefore(Date(timeIntervalSince1970: 1337)))
        XCTAssertFalse(Date(timeIntervalSince1970: 987654321).isBefore(Date(timeIntervalSince1970: 987654321)))
    }
    
    func test_isInThePast() {
        XCTAssert(Date(timeIntervalSinceNow: -0.0001).isInThePast())
        XCTAssert(Date(timeIntervalSinceNow: -1).isInThePast())
        XCTAssert(Date(timeIntervalSinceNow: -10).isInThePast())
        XCTAssert(Date(timeIntervalSinceNow: -1000000).isInThePast())
        
        XCTAssertFalse(Date(timeIntervalSinceNow: 1).isInThePast())
        XCTAssertFalse(Date(timeIntervalSinceNow: 10).isInThePast())
        XCTAssertFalse(Date(timeIntervalSinceNow: 1000).isInThePast())
        XCTAssertFalse(Date(timeIntervalSinceNow: 100000).isInThePast())
    }
}

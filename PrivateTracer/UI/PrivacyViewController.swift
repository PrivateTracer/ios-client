//
//  PrivacyViewController.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee on 16/04/2020.
//

import UIKit

class PrivacyViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var descriptionImageView1: UIImageView!
    @IBOutlet weak var descriptionLabel1: UILabel!
    @IBOutlet weak var descriptionImageView2: UIImageView!
    @IBOutlet weak var descriptionLabel2: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = Strings.App.Privacyview.title
        textLabel.text = Strings.App.Privacyview.label
        descriptionLabel1.text = Strings.App.Privacyview.description1
        descriptionLabel2.text = Strings.App.Privacyview.description2
        
        closeButton.backgroundColor = UIColor.clear
        closeButton.layer.cornerRadius = 10.0
        closeButton.clipsToBounds = true
        closeButton.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.tintColor = .black
        closeButton.setTitle("", for: .normal)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

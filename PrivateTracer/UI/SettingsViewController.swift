//
//  SettingsViewController.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee on 16/04/2020.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var autoMatchSwitch: UISwitch!
    @IBOutlet weak var autoMatchLabel: UILabel!
    @IBOutlet weak var autoMatchDescriptionLabel: UILabel!
    @IBOutlet weak var deleteDataButton: UIButton!
    @IBOutlet weak var deleteDataDescriptionLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        closeButton.backgroundColor = UIColor.clear
        closeButton.layer.cornerRadius = 10.0
        closeButton.clipsToBounds = true
        closeButton.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.tintColor = .black
        closeButton.setTitle("", for: .normal)
        
        deleteDataButton.backgroundColor = UIColor(red: 1.00, green: 0.20, blue: 0.20, alpha: 0.08)
        deleteDataButton.layer.cornerRadius = 10.0
        deleteDataButton.clipsToBounds = true
        deleteDataButton.setTitleColor(UIColor(red: 1.00, green: 0.20, blue: 0.20, alpha: 0.80), for: .normal)
        deleteDataButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        
        titleLabel.text = Strings.App.Settingsview.title
        autoMatchLabel.text = Strings.App.Settingsview.Label.automatch
        autoMatchDescriptionLabel.text = Strings.App.Settingsview.Label.automatchdescription
        deleteDataButton.setTitle(Strings.App.Settingsview.Button.deletealldata, for: .normal)
        deleteDataDescriptionLabel.text = Strings.App.Settingsview.Label.deletealldata
        versionLabel.text = Strings.App.Settingsview.version
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteAllData(_ sender: Any) {
        ServiceInjection.dataManagementService.removeAllData { result in
            var titleText: String?
            var messageText: String?
            switch result {
            case .success(let num):
                titleText = Strings.App.Generic.success
                messageText = Strings.App.Settingsview.Alert.Message.success(num.seeds, num.contacts)
            case .failure(let error):
                titleText = Strings.App.Generic.error
                messageText = error.translation
            }
            let alertController = UIAlertController(
                title: titleText,
                message: messageText,
                preferredStyle: .alert
            )
            
            alertController.addAction(UIAlertAction(title: Strings.App.Generic.close, style: .default, handler: { [weak self] _ in
                self?.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
}

//
//  GradientView.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation
import UIKit

class VerticalGradientView: UIView {
    
    var colors: [(location: Double, color: UIColor)]? {
        didSet {
            setNeedsLayout()
            layoutIfNeeded()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let colors = colors, colors.count >= 2 else { return }
        
        backgroundColor = .clear
        layer.sublayers?.removeAll(where: { $0 is CAGradientLayer })
        
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = colors.map { $0.1.cgColor }
        gradient.locations = colors.map { NSNumber(value: $0.0) }
        
        layer.addSublayer(gradient)
    }
}

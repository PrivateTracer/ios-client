//
//  InfectedInfoViewController.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee on 16/04/2020.
//

import UIKit

class InfectedInfoViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet var numberLoadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var generateCodeButton: UIButton!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var closeButtonTopConstraint: NSLayoutConstraint!

    private let identificationService = ServiceInjection.infectionService
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    private func setupView() {
        generateCodeButton.roundCorners(radius: 10)
        generateCodeButton.setTitle(Strings.App.Infectedview.Button.generate, for: .normal)
        generateCodeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 16)

        titleLabel.text = Strings.App.Infectedview.title
        numberLabel.text = "_ _ _  _ _ _  _ _ _ _"
        textLabel.text = Strings.App.Infectedview.label(PrivateTracerConfig.TTLDays)
        descriptionLabel.text = Strings.App.Infectedview.description

        if UIDevice.isSmallPhone {
            stackView.spacing = 16
            titleLabel.font = UIFont.systemFont(ofSize: 13, weight: .bold)
            numberLabel.font = UIFont.systemFont(ofSize: 35, weight: .bold)
            textLabel.font = UIFont.systemFont(ofSize: 13, weight: .bold)
            descriptionLabel.font = UIFont.systemFont(ofSize: 13)
            closeButtonTopConstraint.constant = 20
        }
    }

    private func setInfectedAndGenerateConfirmationCode() {
        identificationService.setInfected { [weak self] (result) in

            guard let self = self else { return }

            switch result {
            case .success(let confirmationCode):
                self.numberLabel.text = confirmationCode
                self.numberLoadingIndicator.stopAnimating()

            case .failure(let failure):
                self.numberLoadingIndicator.stopAnimating()
                self.showErrorAlert(with: failure.translation)
                self.generateCodeButton.isEnabled = true
            }
        }
    }

    private func showErrorAlert(with message: String) {
        let alertController = UIAlertController(
            title: Strings.App.Generic.error,
            message: message,
            preferredStyle: .alert
        )

        alertController.addAction(UIAlertAction(title: Strings.App.Generic.close, style: .default, handler: { [weak self] _ in
            self?.dismiss(animated: true, completion: nil)
        }))

        self.present(alertController, animated: true, completion: nil)
    }

    @IBAction func generateCodeAction(_ sender: Any) {
        generateCodeButton.isEnabled = false
        numberLoadingIndicator.startAnimating()
        setInfectedAndGenerateConfirmationCode()
    }

    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

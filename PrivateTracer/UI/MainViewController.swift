//
//  MainViewController.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee on 16/04/2020.
//

import UIKit

class MainViewController: UIViewController {

    private enum ViewState {
        case riskyInteraction
        case on
        case off
    }
    
    @IBOutlet var roImageTopConstraint: NSLayoutConstraint!
    @IBOutlet var gradientView: VerticalGradientView!
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var iamInfectedButton: UIButton!
    @IBOutlet var iamInfectedButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!

    private let contactTracingService = ServiceInjection.contactTracingService
    private var subtitleUpdateTimer: Timer?
    private var viewState: ViewState = .off {
        didSet { update(viewState) }
    }

    private func update(_ viewState: ViewState) {
        switch viewState {
        case .riskyInteraction:
            mainLabel.text = Strings.App.Mainview.Label.riskyInteractions
            mainLabel.textColor = UIColor(hex: "EE4444")
            mainButton.setImage(nil, for: .normal)
            infoLabel.text = Strings.App.Mainview.Label.riskyAdvice
            view.backgroundColor = UIColor(hex: "#CE9492")
        case .on:
            mainLabel.text = Strings.App.Mainview.Label.noriskfulinterations
            mainButton.setImage(UIImage(named: "bluetooth_on"), for: .normal)
            mainButton.setTitle(Strings.App.Mainview.Button.setoff, for: .normal)
            infoLabel.text = Strings.App.Mainview.Label.info
            view.backgroundColor = UIColor(hex: "#9BC4D4")
        case .off:
            mainLabel.text = Strings.App.Mainview.Label.noriskfulinterations
            mainButton.setImage(UIImage(named: "bluetooth_off"), for: .normal)
            mainButton.setTitle(Strings.App.Mainview.Button.seton, for: .normal)
            infoLabel.text = Strings.App.Mainview.Label.info
            view.backgroundColor = UIColor(hex: "#C7C7C7")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewState = .off
        
        gradientView.colors = [
            (0.0, UIColor.white.withAlphaComponent(0.95)),
            (1.0, UIColor.white.withAlphaComponent(0))
        ]
        
        if #available(iOS 11.0, *) {
            if view.safeAreaInsets.top > 0 {
                roImageTopConstraint.constant = view.safeAreaInsets.top - 11.0
            }
        }
        
        mainButton.roundCorners(radius: mainButton.bounds.width / 2)
        mainButton.setTitleColor(.black, for: .highlighted)

        refreshButton.tintColor = UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 1.0)

        historyButton.backgroundColor = UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 0.30)
        historyButton.roundCorners(radius: 10)

        iamInfectedButton.backgroundColor = UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 0.30)
        iamInfectedButton.roundCorners(radius: 10)
        iamInfectedButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        iamInfectedButton.setTitle(Strings.App.Mainview.Button.infected, for: .normal)
        if UIDevice.isCompactPhone == false {
            iamInfectedButton.setImage(UIImage(named: "alert_triangle"), for: .normal)
        }
        
        settingsButton.backgroundColor = UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 0.30)
        settingsButton.roundCorners(radius: 10)
        
        infoButton.backgroundColor = UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 0.30)
        infoButton.roundCorners(radius: 10)
        infoButton.setTitle(Strings.App.Mainview.Button.info, for: .normal)
        if UIDevice.isSmallPhone == false {
            infoButton.setImage(UIImage(named: "help_circle"), for: .normal)
        }
        
        if UIDevice.isSmallPhone {
            subtitleLabel.adjustsFontSizeToFitWidth = true
            subtitleLabel.minimumScaleFactor = 0.8
        }
        
        // Immediatly update subtitle
        self.updateSubtitle()
        
        // Immediatly update current infection state
        self.checkInfectionState()
        
        // Check for updates
        ServiceInjection.infectionService.lastUpdateCallback = { [weak self] _ in
            self?.updateSubtitle()
            self?.checkInfectionState()
        }

        #if DEBUG
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(presentHistoryViewController))
        tapGesture.numberOfTapsRequired = 5
        view.addGestureRecognizer(tapGesture)
        #endif

    }

    @objc func presentHistoryViewController() {
        performSegue(withIdentifier: HistoryViewController.identifier, sender: nil)
    }

    @IBAction func mainButtonAction(_ sender: Any) {
        switch viewState {
        case .riskyInteraction:
            // TODO: Decent view state
            break
        case .on:
            contactTracingService.stop()
            viewState = .off
        case .off:
            contactTracingService.start()
            viewState = .on
        }
    }
    
    @IBAction func refreshRiskyInteractions(_ sender: UIButton) {
        ServiceInjection.infectionService.checkForRiskyInteractions { result in
            switch result {
            case .success: break // Don't care, label will be update via other way
            case .failure(let failure):
                
                let alertController = UIAlertController(
                    title: Strings.App.Generic.error,
                    message: failure.translation,
                    preferredStyle: .alert
                )
                
                alertController.addAction(UIAlertAction(title: Strings.App.Generic.close, style: .default, handler: { [weak self] _ in
                    self?.dismiss(animated: true, completion: nil)
                }))
                
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    private func checkInfectionState() {
        
        switch ServiceInjection.infectionService.state {
        case .clean:
            if viewState == .riskyInteraction {
                // Not infected anymore!
                viewState = .off
            }
            
        case .riskyInteractions:
            if viewState == .on {
                // Toggle off
                mainButtonAction(mainButton!)
            }
            viewState = .riskyInteraction
            
            // TODO: Decent view state
            mainButton.setTitle("Stay at home", for: .normal)
            
        case .selfInfected:
            break // TODO: Can't handle this yet
        }
    }

    private func updateSubtitle() {
        
        subtitleUpdateTimer?.invalidate()
        
        // Check if we have data
        let dataAvailable = ServiceInjection.dp3tService.epochAndSeed != nil
        refreshButton.isHidden = dataAvailable == false
        guard dataAvailable else {
            subtitleLabel.text = Strings.App.Mainview.Label.begintracing
            return
        }
       
        // Check if we have already updated before
        guard let lastUpdate = ServiceInjection.infectionService.lastTraceSetID?.dateFetched else {
            subtitleLabel.text = Strings.App.Mainview.Label.notYet
            return
        }
        
        subtitleLabel.text = Strings.App.Mainview.Label.lastupdated + " \(lastUpdate.timeAgoDescription())"
        
        // Update periodically for "time ago" updates
        subtitleUpdateTimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { (_) in
            self.updateSubtitle()
        }
    }
}

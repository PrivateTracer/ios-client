//
//  HistoryViewController.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee on 16/04/2020.
//

import UIKit

class HistoryViewController: UIViewController {
    
    @IBOutlet weak var seedHistoryLabel: UITextView!
    @IBOutlet weak var contactHistoryLabel: UITextView!
    @IBOutlet weak var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        closeButton.backgroundColor = UIColor.clear
        closeButton.layer.cornerRadius = 10.0
        closeButton.clipsToBounds = true
        closeButton.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.tintColor = .black
        closeButton.setTitle("", for: .normal)
        
        let seedRecordsService = ServiceInjection.seedRecordsService
        seedRecordsService.seedHistory { [weak self] result in
            switch result {
            case .success(let seeds):
                var historyLabelText = seeds.reduce("") { label, seedRecord in
                    return label + "\(seedRecord.epochId), \(seedRecord.seed), \(dateFormatter.string(from: seedRecord.timestamp))" + "\n"
                }
                if historyLabelText == "" {
                    historyLabelText = Strings.App.Historyview.Textview.seedhistory
                }
                self?.seedHistoryLabel.text = historyLabelText
            case .failure(let reason):
                self?.seedHistoryLabel.text = "Error: \(reason)"
            }
        }
        
        let contactRecordService = ServiceInjection.contactRecordingService
        contactRecordService.recordedContacts {[weak self] result in
            switch result {
            case .success(let contactHistory):
                var contactHistoryLabelText = contactHistory.reduce("") { label, contact in
                    return label + "\(dateFormatter.string(from: contact.timestamp)), \(contact.identity)\n"
                }
                if contactHistoryLabelText == "" {
                    contactHistoryLabelText = Strings.App.Historyview.Textview.contacthistory
                }
                self?.contactHistoryLabel.text = contactHistoryLabelText
            case .failure(let reason):
                self?.contactHistoryLabel.text = "Error: \(reason)"
            }
        }
    }
    
    @IBAction func dismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

//
//  Dictionary + Extensions.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 21/04/2020.
//

import Foundation

extension Dictionary {
    mutating func merge(dict: [Key: Value]) {
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}

//
//  String+Extensions.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 24/04/2020.
//

import Foundation

extension Collection {
    func distance(to index: Index) -> Int { distance(from: startIndex, to: index) }
    func distance(from index: Index) -> Int { distance(from: index, to: endIndex) }
}

extension StringProtocol where Self: RangeReplaceableCollection {
    mutating func insert<S: StringProtocol>(separator: S, every n: Int, minLength length: Int) {
        for index in indices.dropFirst().reversed()
            where distance(to: index).isMultiple(of: n) && distance(from: index) >= length {
            insert(contentsOf: separator, at: index)
        }
    }
    func inserting<S: StringProtocol>(separator: S, every n: Int, minLength length: Int) -> Self {
        var string = self
        string.insert(separator: separator, every: n, minLength: length)
        return string
    }
}

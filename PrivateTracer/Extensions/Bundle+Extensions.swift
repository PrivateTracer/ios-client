//
//  Bundle+Extensions.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee.
//

import Foundation

extension Bundle {

    var displayName: String? {
        return object(forInfoDictionaryKey: "CFBundleName") as? String
    }
}

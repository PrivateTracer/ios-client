//
//  UIView+Extensions.swift
//  PrivateTracer
//
//  Created by Fouad Astitou on 02/05/2020.
//

import UIKit

extension UIView {
    func roundCorners(radius: CGFloat) {
        layer.cornerRadius = radius
        clipsToBounds = true
    }
}

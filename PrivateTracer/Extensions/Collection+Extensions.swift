//
//  Collection+Extensions.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation

extension Collection {
    
    /// NOT THREADSAFE: Returns the element at the specified index if it is within bounds, otherwise nil.
    ///
    /// - Parameter index: duh, the index!
    public subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

//
//  Date+Extensions.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation

extension Date {
    
    public func isBefore(_ rhs: Date) -> Bool {
        return self.compare(rhs) == .orderedAscending
    }

    public func isInThePast() -> Bool {
        return isBefore(Date())
    }
    
    @available(iOS 13.0, *) static let relativeDateFormatter: RelativeDateTimeFormatter = {
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = .full
        return formatter
    }()
    
    public func timeAgoDescription() -> String {
                
        // Hard way
        let calendar = Calendar.current
        let minuteAgo = calendar.date(byAdding: .minute, value: -1, to: Date())!
        let hourAgo = calendar.date(byAdding: .hour, value: -1, to: Date())!
        let dayAgo = calendar.date(byAdding: .day, value: -1, to: Date())!

        if minuteAgo < self {
            let diff = Calendar.current.dateComponents([.second], from: self, to: Date()).second ?? 0
            
            if diff < 10 {
                return Strings.Timeago.justNow
            } else {
                return "\(diff) \(Strings.Timeago.seconds)"
            }
        } else if hourAgo < self {
            let diff = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute ?? 0
            
            if diff < 2 {
                return "\(diff) \(Strings.Timeago.minute)"
            } else {
                return "\(diff) \(Strings.Timeago.minutes)"
            }
            
        } else if dayAgo < self {
            let diff = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour ?? 0
            
            if diff < 2 {
                return "\(diff) \(Strings.Timeago.hour)"
            } else {
                return "\(diff) \(Strings.Timeago.hours)"
            }
        }
        
        let diff = Calendar.current.dateComponents([.day], from: self, to: Date()).day ?? 0

        if diff < 2 {
            return "\(diff) \(Strings.Timeago.day)"
        } else {
            return "\(diff) \(Strings.Timeago.days)"
        }
    }
}

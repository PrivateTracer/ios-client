//
//  Array+Extensions.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 20/04/2020.
//

import Foundation

extension Array {
    var uuid: UUID? {
        if let uintArray = self as? [UInt8], uintArray.count == 16 {
            return UUID(uuid: uuid_t((uintArray[0], uintArray[1], uintArray[2], uintArray[3],
                                      uintArray[4], uintArray[5], uintArray[6], uintArray[7],
                                      uintArray[8], uintArray[9], uintArray[10], uintArray[11],
                                      uintArray[12], uintArray[13], uintArray[14], uintArray[15])))
        }
        return nil
    }
    
    var uInt8Tuple: HashedIdentity? {
        if let uintArray = self as? [UInt8], uintArray.count == 32 {
            return (
                uintArray[0], uintArray[1], uintArray[2], uintArray[3],
                uintArray[4], uintArray[5], uintArray[6], uintArray[7],
                uintArray[8], uintArray[9], uintArray[10], uintArray[11],
                uintArray[12], uintArray[13], uintArray[14], uintArray[15],
                uintArray[16], uintArray[17], uintArray[18], uintArray[19],
                uintArray[20], uintArray[21], uintArray[22], uintArray[23],
                uintArray[24], uintArray[25], uintArray[26], uintArray[27],
                uintArray[28], uintArray[29], uintArray[30], uintArray[31]
            )
        }
        return nil
    }
}

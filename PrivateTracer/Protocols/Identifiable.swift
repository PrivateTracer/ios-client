//
//  Identifiable.swift
//  PrivateTracer
//
//  Created by Fouad Astitou on 02/05/2020.
//

import Foundation

protocol Identifiable: AnyObject {}

extension Identifiable {
    static var identifier: String { String(describing: self) }
}


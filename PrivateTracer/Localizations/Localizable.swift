// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum Strings {

  internal enum App {
    internal enum Generic {
      /// Close
      internal static let close = Strings.tr("Localizable", "app.generic.close")
      /// Something went wrong
      internal static let error = Strings.tr("Localizable", "app.generic.error")
      /// Success
      internal static let success = Strings.tr("Localizable", "app.generic.success")
      internal enum Errors {
        /// Request timed out, please make sure your phone has reception
        internal static let networkTimeout = Strings.tr("Localizable", "app.generic.errors.networkTimeout")
        /// Please check you are connected to the internet
        internal static let noInternet = Strings.tr("Localizable", "app.generic.errors.noInternet")
        /// Request failed, please try again later
        internal static let unknown = Strings.tr("Localizable", "app.generic.errors.unknown")
      }
    }
    internal enum Historyview {
      internal enum Textview {
        /// No contacts detected yet
        internal static let contacthistory = Strings.tr("Localizable", "app.historyview.textview.contacthistory")
        /// Tracing wasn't switched on yet
        internal static let seedhistory = Strings.tr("Localizable", "app.historyview.textview.seedhistory")
      }
    }
    internal enum Infectedview {
      /// With this code, the doctor can give the key to give an anonymous notification to all devices with which you have had risky interactions in the past 14 days
      internal static let description = Strings.tr("Localizable", "app.infectedview.description")
      /// Please note, as soon as you generate a code, all interactions that you have had in the past %d days are sent to the central server. A healthcare provider can use your unique code to mark your interactions as risky
      internal static func label(_ p1: Int) -> String {
        return Strings.tr("Localizable", "app.infectedview.label", p1)
      }
      /// Fetching confirmation code...
      internal static let loading = Strings.tr("Localizable", "app.infectedview.loading")
      /// Do you think you are infected?
      internal static let title = Strings.tr("Localizable", "app.infectedview.title")
      internal enum Button {
        /// Generate code
        internal static let generate = Strings.tr("Localizable", "app.infectedview.button.generate")
      }
      internal enum Error {
        /// You have not used the app yet, so we can't inform other users
        internal static let noTraces = Strings.tr("Localizable", "app.infectedview.error.noTraces")
      }
    }
    internal enum Mainview {
      internal enum Button {
        /// I am infected
        internal static let infected = Strings.tr("Localizable", "app.mainview.button.infected")
        /// What details are recorded?
        internal static let info = Strings.tr("Localizable", "app.mainview.button.info")
        /// Turn Off
        internal static let setoff = Strings.tr("Localizable", "app.mainview.button.setoff")
        /// Turn On
        internal static let seton = Strings.tr("Localizable", "app.mainview.button.seton")
      }
      internal enum Label {
        /// Start tracing to update your status
        internal static let begintracing = Strings.tr("Localizable", "app.mainview.label.begintracing")
        /// Set tracing on if you go out of the house
        internal static let info = Strings.tr("Localizable", "app.mainview.label.info")
        /// Last updated:
        internal static let lastupdated = Strings.tr("Localizable", "app.mainview.label.lastupdated")
        /// No risky interactions detected
        internal static let noriskfulinterations = Strings.tr("Localizable", "app.mainview.label.noriskfulinterations")
        /// Not checked yet
        internal static let notYet = Strings.tr("Localizable", "app.mainview.label.notYet")
        /// Contact a doctor when you have symptoms.
        internal static let riskyAdvice = Strings.tr("Localizable", "app.mainview.label.riskyAdvice")
        /// Risky interactions have been detected
        internal static let riskyInteractions = Strings.tr("Localizable", "app.mainview.label.riskyInteractions")
      }
    }
    internal enum Privacyview {
      /// There is only one central server, but there is not going to be anything that can lead to you. Hackers have no use hacking the central server.
      internal static let description1 = Strings.tr("Localizable", "app.privacyview.description1")
      /// All critical operations are performed and stored locally on your device. No one else can access them.
      internal static let description2 = Strings.tr("Localizable", "app.privacyview.description2")
      /// There is no central storage of your personal information, so we guarantee your privacy and security.
      internal static let label = Strings.tr("Localizable", "app.privacyview.label")
      /// What data is being shared?
      internal static let title = Strings.tr("Localizable", "app.privacyview.title")
    }
    internal enum Settingsview {
      /// Settings
      internal static let title = Strings.tr("Localizable", "app.settingsview.title")
      /// Version 1.0.0
      internal static let version = Strings.tr("Localizable", "app.settingsview.version")
      internal enum Alert {
        internal enum Message {
          /// Deleted %d anonymous IDs and %d recorded contacts.
          internal static func success(_ p1: Int, _ p2: Int) -> String {
            return Strings.tr("Localizable", "app.settingsview.alert.message.success", p1, p2)
          }
        }
      }
      internal enum Button {
        /// Delete all data
        internal static let deletealldata = Strings.tr("Localizable", "app.settingsview.button.deletealldata")
      }
      internal enum Label {
        /// Auto match
        internal static let automatch = Strings.tr("Localizable", "app.settingsview.label.automatch")
        /// If you check this option, the app will automatically check every 6 hours for risky interactions that match your interactions.
        internal static let automatchdescription = Strings.tr("Localizable", "app.settingsview.label.automatchdescription")
        /// This will permanently delete all data in this application.
        internal static let deletealldata = Strings.tr("Localizable", "app.settingsview.label.deletealldata")
      }
    }
  }

  internal enum Timeago {
    /// day ago
    internal static let day = Strings.tr("Localizable", "timeago.day")
    /// days ago
    internal static let days = Strings.tr("Localizable", "timeago.days")
    /// hour ago
    internal static let hour = Strings.tr("Localizable", "timeago.hour")
    /// hours ago
    internal static let hours = Strings.tr("Localizable", "timeago.hours")
    /// just now
    internal static let justNow = Strings.tr("Localizable", "timeago.justNow")
    /// minute ago
    internal static let minute = Strings.tr("Localizable", "timeago.minute")
    /// minutes ago
    internal static let minutes = Strings.tr("Localizable", "timeago.minutes")
    /// seconds ago
    internal static let seconds = Strings.tr("Localizable", "timeago.seconds")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension Strings {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}

//
//  PlistHelper.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 17/04/2020.
//
import Foundation

struct PlistHelper {
    static func getStringValueFromInfoPlist(withKey key: String) -> String? {
        if  let path = Bundle.main.path(forResource: "Info", ofType: "plist"),
            let keyValue = NSDictionary(contentsOfFile: path)?.value(forKey: key) as? String {
            return keyValue
        }
        return nil
    }
    static func getIntValueFromInfoPlist(withKey key: String) -> Int? {
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist"),
            let keyValue = NSDictionary(contentsOfFile: path)?.value(forKey: key) as? Int {
            return keyValue
        }
        return nil
    }

}

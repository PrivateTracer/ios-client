//
//  UIDevice+Extensions.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import UIKit

extension UIDevice {
    
    /// Compact phone is smaller than iPhone XR
    public static let isCompactPhone: Bool = {
        // We use min(height, width) to get the width even in landscape
        min(UIScreen.main.bounds.height, UIScreen.main.bounds.width) < 414.0 // 414 is the width iPhone xr, max and plus and similar
    }()
    
    /// Small phone is iPhone 5 / 5s / 5c / SE and smaller (4s)
    public static let isSmallPhone: Bool = {
        // We use min(height, width) to get the width even in landscape
        min(UIScreen.main.bounds.height, UIScreen.main.bounds.width) <= 320.0
    }()
    
}

//
//  LogHelper.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation

enum LogHelper {
    
    private static let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.ss ZZZZZ"
        return dateFormatter
    }()
    
    /// Log a message to console, only prints in debug builds.
    /// - Parameter message: Message to send to console
    static func debugLog(_ message: String) {
        #if DEBUG
        print("[\(dateFormatter.string(from: Date()))] \(message)")
        #endif
    }
}

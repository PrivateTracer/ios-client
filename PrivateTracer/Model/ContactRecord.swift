//
//  ContactRecord.swift
//  PrivateTracer
//
//  Created by Stijn Spijker on 18/04/2020.
//

import Foundation
import CoreData

/// A contact record is the record of an ephemeral identifier (another user) we
/// encountered for a prolonged time in the form of a HashedIdentity.
@objc(ContactRecord) public class ContactRecord: NSManagedObject {
    @NSManaged public var identity: String
    @NSManaged public var timestamp: Date
    
    /// Hide this under the carpet, it is too ugly for daylight
    lazy var rawHashedIdentity: HashedIdentity? = {
        let rawIdentityData = Array(Data(base64Encoded: identity)!)
        
        return rawIdentityData.uInt8Tuple
    }()
    
}

extension ContactRecord: Encodable {
    enum CodingKeys: String, CodingKey {
        case identity
        case timestamp
    }
    
    // MARK: - Encodable
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(identity, forKey: .identity)
        try container.encode(Int(timestamp.timeIntervalSince1970), forKey: .timestamp)
    }
}

//
//  File.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 17/04/2020.
//
import CoreBluetooth
import Foundation

/// Save all parameters here that we might need to change
/// TODO: This will come from a server and should be saved to UserDefaults
struct PrivateTracerConfig {
    /// How long do we keep the data
    static let TTLDays = PlistHelper.getIntValueFromInfoPlist(withKey: "RETAIN_DATA_DAYS") ?? 14
    
    /// How long do we stay inside
    static let QuarantinePeriodInDays = PlistHelper.getIntValueFromInfoPlist(withKey: "SELF_QUARANTINE_PERIOD") ?? 14
    
    /// How often do we check for risky interactions / fetch new tracesets
    static let TracesetUpdatePeriodInSeconds = TimeInterval(PlistHelper.getIntValueFromInfoPlist(withKey: "TRACESET_UPDATE_PERIOD") ?? 300)
    
    /// URL of server for fetching and submitting infections
    #if DEV || MOCK
    static let BaseURL = URL(string: "https://mobileclientservicesdev.azurewebsites.net:443/")!
    #elseif LOCAL
    static let BaseURL = URL(string: "https://127.0.0.1:5000/")!
    #elseif DEMO
    static let BaseURL = URL(string: "https://mobileclientservicesdev.azurewebsites.net/")!
    #elseif PROD
    static let BaseURL = URL(string: "https://client-services-prd.documents.azure.com:443/")!
    #endif
    
    /// UUID of the service channel to use in Bluetooth broadcast
    static let BluetoothServiceID = CBUUID(string: "\(PlistHelper.getStringValueFromInfoPlist(withKey: "BT_SERVICE_ID") ?? "00001D1D-0000-1000-8000-00805F9B34FB")")
    static let ShortBluetoothServiceID = CBUUID(string: "\(PlistHelper.getStringValueFromInfoPlist(withKey: "BT_SHORT_SERVICE_ID") ?? "1D1D")")
    
    /// Contact scanning parameters to determine risky contacts
    /// How often and how long to scan for peripherals
    static let CentralScanInterval = 30
    static let CentralScanDuration = 10
    /// Minimum duration during which there were observations
    static let RiskyContactMinimumDuration = 0.0
    /// Minimum rssi for an observation to be valid
    static let RiskyContactMinimumRssi: Double = -65
}

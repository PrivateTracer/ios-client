//
//  InfectionState.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation

enum InfectionState {
    case clean
    case riskyInteractions(lastRiskyInteraction: Date)
    case selfInfected
}

extension InfectionState: Comparable {
    static func < (lhs: InfectionState, rhs: InfectionState) -> Bool {
        
        switch (lhs, rhs) {
        case (.clean, .clean): return false  // Preserve order
        case (.clean, .riskyInteractions): return true
        case (.clean, .selfInfected): return true
            
        case (.riskyInteractions, .clean): return false
        case (.riskyInteractions(let lhs_date), .riskyInteractions(let rhs_date)):
            return lhs_date.isBefore(rhs_date) // Oldest date is least important
        case (.riskyInteractions, .selfInfected): return true
            
        case (.selfInfected, .clean): return false
        case (.selfInfected, .riskyInteractions): return false
        case (.selfInfected, .selfInfected): return false // Preserve order
        }
        
    }
}

extension InfectionState: Codable {
    enum CodingKeys: String, CodingKey {
        case baseState
        case lastRiskyInteraction
    }
    
    enum DecodingError: Error {
        case unknownState
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let baseState = try container.decode(String.self, forKey: .baseState)
        switch baseState {
        case "riskyInteractions":
            let lastRiskyInteraction = try container.decode(Date.self, forKey: .lastRiskyInteraction)
            self = .riskyInteractions(lastRiskyInteraction: lastRiskyInteraction)
            
        case "selfInfected":
            self = .selfInfected
            
        case "clean":
            self = .clean
            
        default:
            throw DecodingError.unknownState
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        switch self {
        case .clean:
            try container.encode("clean", forKey: .baseState)
            
        case .riskyInteractions(let lastRiskyInteraction):
            try container.encode("riskyInteractions", forKey: .baseState)
            try container.encode(lastRiskyInteraction, forKey: .lastRiskyInteraction)
            
        case .selfInfected:
            try container.encode("selfInfected", forKey: .baseState)
        }
    }
}

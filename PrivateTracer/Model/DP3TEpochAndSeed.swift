//
//  DP3TEpochInfo.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation
import UIKit
import CoreData

struct DP3TEpochAndSeed: Codable, CustomDebugStringConvertible {
    
    /// This Epoch's identifier
    let id: Epoch
    
    /// The seed for this Epoch
    let seed: Data
    
    /// The date (unix timestamp, UTC) after which the app should get a new Epoch.
    let nextAfter: Date
    
    var debugDescription: String {
        let secondsLeft = Int(nextAfter.timeIntervalSince(Date())) // Throw away the decimals
        return "Epoch: \(id), valid until: \(nextAfter), that's \(secondsLeft) seconds"
    }
    
    func saveToCoreData(completion: @escaping (Result<Void, DataError>) -> Void) {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        appDelegate.persistentContainer.performBackgroundTask { managedContext in
            let entity = NSEntityDescription.entity(forEntityName: "SeedRecord", in: managedContext)!
            let seedRecord = SeedRecord(entity: entity, insertInto: managedContext)
            seedRecord.set(seedStruct: self)
            do {
                try managedContext.save()
                DispatchQueue.main.async {
                    completion(.success(()))
                }
            } catch let error as NSError {
                LogHelper.debugLog("[DP3TEpochAndSeed model] Could not save. \(error), \(error.userInfo)")
                completion(.failure(.system(localizedDescription: error.localizedDescription)))
            }
        }
    }
}

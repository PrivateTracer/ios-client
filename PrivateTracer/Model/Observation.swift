//
//  Observation.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 18/04/2020.
//

import Foundation
import CoreBluetooth

/// A temporary observation (only kept in memory, not saved)
struct Observation {
    let ephId: UUID?
    let timestamp: Date
    let txLevel: NSNumber?
    let rssi: NSNumber?
    
    init(ephId: UUID?, timestamp: Date, txLevel: NSNumber?, rssi: NSNumber?) {
        self.ephId = ephId
        self.timestamp = timestamp
        self.txLevel = txLevel
        self.rssi = rssi
    }
    
    init(fromCBUUID cbuuid: CBUUID, timestamp: Date) {
        let ephId = UUID(uuidString: cbuuid.uuidString)
        self.init(ephId: ephId, timestamp: timestamp, txLevel: nil, rssi: nil)
    }
}

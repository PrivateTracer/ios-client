//
//  ApiTracer.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation

struct ApiTracer: Codable {
    let token: String
    let items: [ApiTracerKey]
    
    init(with token: String, and seeds: [SeedRecord]) {
        self.token = token
        
        items = seeds.map { seedRecord -> ApiTracerKey in
            ApiTracerKey(with: seedRecord)
        }
    }
}

struct ApiTracerKey: Codable {
    let seed: String
    let epoch: Int32
    
    init(with seedRecord: SeedRecord) {
        self.seed = seedRecord.seed
        self.epoch = seedRecord.epochId
    }
}

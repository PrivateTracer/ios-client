//
//  SeedRecord.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 17/04/2020.
//

import Foundation
import CoreData

/// A seed record is the record of an epoch and seed that was broadcasted by an ephemeral identifier
@objc(SeedRecord) public class SeedRecord: NSManagedObject {
    @NSManaged public var seed: String
    @NSManaged public var epochId: Int32
    @NSManaged public var timestamp: Date
    
    lazy var seedArray: [UInt8] = {
        guard let seedData = Data(base64Encoded: self.seed) else { return [] }
        return Array(seedData)
    }()
    
    func set(seedStruct: DP3TEpochAndSeed) {
        self.seed = seedStruct.seed.base64EncodedString()
        self.epochId = seedStruct.id
        self.timestamp = Date()
    }
}

extension SeedRecord: Encodable {
    enum CodingKeys: String, CodingKey {
        case seed
        case epochId
        case timestamp
    }
    
    // MARK: - Encodable
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(Int(truncating: NSNumber(value: epochId)), forKey: .epochId)
        try container.encode(seed, forKey: .seed)
        try container.encode(Int(timestamp.timeIntervalSince1970), forKey: .timestamp)
    }
}

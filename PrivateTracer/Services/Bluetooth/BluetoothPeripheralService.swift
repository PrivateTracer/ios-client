//
//  BluetoothPeripheralService.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee on 17/04/2020.
//

import Foundation

/// Provides the bluetooth broadcast service
/// This will broadcast the service found in `PrivateTracerConfig.BluetoothServiceID` with a characteristic of which the UUID is the ephemeral ID
protocol BluetoothPeripheralService {
        
    /// Start broadcasting
    /// - Parameter _: The ephemeralID for the current epoch
    func startWithEphID(_: UUID)
    
    /// Stop broadcasting
    func stop()
    
}

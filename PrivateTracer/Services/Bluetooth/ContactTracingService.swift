//
//  ObservationManager.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 18/04/2020.
//

import Foundation

/// The main service to use when the user starts contact tracing in the app
protocol ContactTracingService {
    
    /// Start tracing: This will start scanning **and** broadcasting
    func start()
    
    /// Stop tracing
    func stop()
}

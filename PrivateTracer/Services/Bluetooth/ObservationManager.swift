//
//  ContactManager.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 18/04/2020.
//

import Foundation
import CoreBluetooth

/// A protocol for communicating with the `ContactTracingService`
protocol ObservationManager {
        
    /// Add an observation to memory whenever there is an observation recorded
    /// - Parameter uuid: The UUID of the peripheral for which the observation was made, equals `CBPeripheral.identity`
    /// - Parameter peripheral: The peripheral for which the observation was made
    /// - Parameter values: 
    func addObservationWithUuid(_ uuid: UUID, values: Observation)
    
    /// Check if the observation already exists, that way the ephId isn't needed
    /// - Parameter uuid: The UUID of the peripheral for which we are requesting observations
    /// - Returns: true if there was already an observation with EphID recorded
    func hasObservationWithUuid(_ uuid: UUID) -> Bool
}

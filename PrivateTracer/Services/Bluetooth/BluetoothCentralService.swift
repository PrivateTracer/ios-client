//
//  BluetoothCentralService.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 18/04/2020.
//

import Foundation

/// Provides the bluetooth scanning service
protocol BluetoothCentralService {
    
    /// Start scanning for other PrivateTracer Bluetooth devices
    /// - Parameter withObservationManager: The manager object to which observations are sent using the `addObservation` method.
    func start(withObservationManager: ObservationManager)
    
    /// Stop scanning
    func stop()
        
}

//
//  ApiService.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation

enum SubmitTraceSetFailure: Error {
    case noInternet
    case timeout
    case system(localizedDescription: String)
    case unknown
    case noTraces
    
    var translation: String {
        switch self {
        case .noInternet: return Strings.App.Generic.Errors.noInternet
        case .timeout: return Strings.App.Generic.Errors.networkTimeout
        case .system(let localizedDescription): return localizedDescription
        case .unknown: return Strings.App.Generic.Errors.unknown
        case .noTraces: return Strings.App.Infectedview.Error.noTraces
        }
    }
}

enum LastTraceSetIDFailure: Error {
    case noInternet
    case timeout
    case system(localizedDescription: String)
    case unknown
    case noTraceSets
    
    var translation: String {
        switch self {
        case .noInternet: return Strings.App.Generic.Errors.noInternet
        case .timeout: return Strings.App.Generic.Errors.networkTimeout
        case .system(let localizedDescription): return localizedDescription
        case .unknown: return Strings.App.Generic.Errors.unknown
        case .noTraceSets: return Strings.App.Generic.Errors.unknown
        }
    }
}

enum TraceSetFailure: Error {
    case noInternet
    case timeout
    case system(localizedDescription: String)
    case unknown
    case notFound
    
    var translation: String {
        switch self {
        case .noInternet: return Strings.App.Generic.Errors.noInternet
        case .timeout: return Strings.App.Generic.Errors.networkTimeout
        case .system(let localizedDescription): return localizedDescription
        case .unknown: return Strings.App.Generic.Errors.unknown
        case .notFound: return Strings.App.Generic.Errors.unknown
        }
    }
}

protocol ApiService {

    /// Call to get the amount of tracesets available right now
    /// - Parameter completionBlock: Result with either a failure, or the id of the last traceset
    @discardableResult func traceSetCount(completionBlock: @escaping (Result<Int, LastTraceSetIDFailure>) -> Void) -> URLSessionDataTask?
    
    /// Get a traceset with its identifier
    /// - Parameters:
    ///   - id: The identifier of the traceset
    ///   - completionBlock: Result with traceset Json data when success
    @discardableResult func traceset(_ id: Int, completionBlock: @escaping (Result<Data, TraceSetFailure>) -> Void) -> URLSessionDataTask?

    /// Call to submit a set of Epoch's and seeds to the server for a doctor to verify.
    /// - Parameters:
    ///   - infectedSeeds: Seeds you broadcasted in the X days
    ///   - completionBlock: Result with either a failure, or the confirmation code for the doctor
    @discardableResult func submitTraceSet(for infectedSeeds: [SeedRecord], completionBlock: @escaping (Result<String, SubmitTraceSetFailure>) -> Void) -> URLSessionDataTask?
}

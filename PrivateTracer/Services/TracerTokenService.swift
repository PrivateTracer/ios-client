//
//  AuthCodeService.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 24/04/2020.
//

import Foundation

/// Class that generates and validates a token according the documentation found here
/// https://gitlab.com/PrivateTracer/caregiversportal/-/blob/develop/AuthenticationCodes.md
protocol TracerTokenGenerator {
    
    /// Generates a cryptographically secure random token for tracesets
    /// The last character is a check character that can be used to validate the token
    /// - Returns: A token string
    func newTracerToken() -> String
    
    /// Validates the tracer token (i.e. that check code is correct)
    /// - Parameter _: The token string
    func isValidTracerToken(_: String) -> Bool
}

//
//  MockInfectionService.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation

class MockInfectionService: InfectionService {
    
    var lastTraceSetID: LastTracesetFetch? = (Date(timeIntervalSinceNow: -30), 1)
    var state: InfectionState = .clean

    var lastUpdateCallback: ((Date?) -> Void)? {
        didSet {
            lastUpdateCallback?(lastTraceSetID?.dateFetched)
        }
    }
    
    func checkForRiskyInteractions(completionBlock: ((Result<Date?, TraceSetFailure>) -> Void)?) {
        let isLuckyDay = Int.random(in: 0...5) == 0
        
        guard isLuckyDay else {
            completionBlock?(.success(Date()))
            return
        }
        
        let daysAgo = Double.random(in: 1...13)
        let secondsAgo = daysAgo * 86400
        
        state = .riskyInteractions(lastRiskyInteraction: Date(timeIntervalSinceNow: -secondsAgo))
        completionBlock?(.success(Date()))
    }

    func setInfected(completionBlock: (Result<String, SubmitTraceSetFailure>) -> Void) {
        state = .selfInfected
        completionBlock(.success("MOC-MOC-MOCK"))
    }
    
    func resetAll() {
        // do nothing
    }
}

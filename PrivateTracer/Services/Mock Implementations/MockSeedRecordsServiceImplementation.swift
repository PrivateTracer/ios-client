//
//  MockSeedRecordsServiceImplematation.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 17/04/2020.
//
import CoreData
import UIKit
import Foundation

class MockSeedRecordsServiceImplementation: SeedRecordsService {
    func seedHistory(completionBlock: @escaping (Result<[SeedRecord], DataError>) -> Void) {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                completionBlock(.failure(.unknown))
                return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "SeedRecord", in: managedContext)!
        let seedRecord = SeedRecord(entity: entity, insertInto: managedContext)
        seedRecord.epochId = 1337
        seedRecord.seed = "a52R28o6qFk3aJAf552CwBQbPoNAA9TdTVducJY1roE="
        seedRecord.timestamp = Date()
        completionBlock(.success([seedRecord]))
    }
    
    func removeOldData(completionBlock: @escaping (Result<Int, DataError>) -> Void) {
        completionBlock(.success(1))
    }
    
    func removeAllData(completionBlock: @escaping (Result<Int, DataError>) -> Void) {
        completionBlock(.success(2))
    }
}

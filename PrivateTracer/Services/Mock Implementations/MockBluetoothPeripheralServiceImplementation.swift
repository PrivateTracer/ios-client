//
//  MockBluetoothPeripheralService.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee on 17/04/2020.
//

import Foundation

class MockBluetootPeripheralServiceImplementation: BluetoothPeripheralService {
    
    func startWithEphID(_: UUID) {
        
    }
     
    func stop() {
        
    }
}

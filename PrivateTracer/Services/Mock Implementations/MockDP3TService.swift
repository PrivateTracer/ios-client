//
//  MockDP3TServiceImplementation.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation

class MockDP3TServiceImplementation: DP3TService {
    
    func start(epochChangeCallback: @escaping ((UUID) -> Void)) {
        self.epochChangeCallback = epochChangeCallback
    }
    
    func stop() {
        epochChangeCallback = nil
    }
    
    func resetAll() {
        // do nothing
    }
    
    private var epochChangeCallback: ((UUID) -> Void)? {
        didSet {
            let uuid = UUID(uuidString: "A6BA4286-C550-4794-A888-9467EF0B31A8")!
            epochChangeCallback?(
                uuid
            )
        }
    }
    
    var epochAndSeed: DP3TEpochAndSeed? {
        return DP3TEpochAndSeed(
            id: 1337,
            seed: Data(repeating: 2, count: 32),
            nextAfter: Date(timeIntervalSinceNow: 999999)
        )
    }
    
    func newTracerToken() -> String {
        return "12345"
    }
    
    func generateHashedIdentityBase64(ephemeralIdentifier: UUID, epochIdentifier: Epoch) -> String {
        return "MOCKED/HASHED/IDENTITY="
    }
    
    func checkForRiskyInteractions(contacts: [ContactRecord], tracesetJsonData: Data) -> Date? {
        
        let isLuckyDay = Int.random(in: 0...5) == 0
        
        guard isLuckyDay else {
            return nil
        }
        
        let daysAgo = Double.random(in: 1...13)
        let secondsAgo = daysAgo * 86400
        return Date(timeIntervalSinceNow: -secondsAgo)
    }
}

//
//  MockApiService.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation

class MockApiService: ApiService {
    
    @discardableResult func traceset(_ id: Int, completionBlock: @escaping (Result<Data, TraceSetFailure>) -> Void) -> URLSessionDataTask? {
        completionBlock(.failure(.notFound))
        return nil
    }
    
    @discardableResult func traceSetCount(completionBlock: @escaping (Result<Int, LastTraceSetIDFailure>) -> Void) -> URLSessionDataTask? {
        completionBlock(.success(1))
        return nil
    }
    
    @discardableResult func submitTraceSet(for infectedSeeds: [SeedRecord], completionBlock: @escaping (Result<String, SubmitTraceSetFailure>) -> Void) -> URLSessionDataTask? {
        completionBlock(Result<String, SubmitTraceSetFailure>.success("123-123-1337"))
        return nil
    }
    
}

//
//  InfectionService.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation

/// Handles all infection flow
protocol InfectionService {
    
    /// Current state of infection
    var state: InfectionState { get }
    
    /// Use this callback to receive updates about the last
    /// risky interaction check. If nil, it has never been checked,
    /// because it was not necessary
    /// You can also use this callback to check if the infection state changed!
    var lastUpdateCallback: ((Date?) -> Void)? { get set }

    /// Will mark current stored seeds / epochs as infected and request a confirmation code for it.
    /// - Parameter completionBlock: Result, with success confirmation code, or failure localized error.
    func setInfected(completionBlock: @escaping (Result<String, SubmitTraceSetFailure>) -> Void)
    
    /// Provides you with a date of last check
    var lastTraceSetID: LastTracesetFetch? { get }
    typealias LastTracesetFetch = (dateFetched: Date, lastID: Int)
    
    /// Start / force checking for risky interactions
    /// Call this when starting bluetooth broadcasting / monitoring.
    /// This will start downloading and matching contacts against tracesets for risky interactions
    /// Will start automatically on app launch after the first time it is called
    /// - Parameter completionBlock: Optional, provide if user initiated, last updated date when succes.
    func checkForRiskyInteractions(completionBlock: ((Result<Date?, TraceSetFailure>) -> Void)?)
    
    /// Reset all data and state
    func resetAll()
}

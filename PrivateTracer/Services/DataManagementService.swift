//
//  DataManagementService.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 29/04/2020.
//

import Foundation

enum DataError: Error {
    case unknown
    case system(localizedDescription: String)
    
    var translation: String {
        switch self {
        case .unknown: return Strings.App.Generic.Errors.unknown
        case .system(let description): return description
        }
    }
}

/// Provides functionality to delete the data stored by the app
protocol DataManagementService {
    
    /// Delete all data from the app. This includes:
    /// - Seed records
    /// - Contact records
    /// - Resets the infection status
    /// - Deletes the last tracing set counter that was fetched
    func removeAllData(completion: @escaping (Result<(seeds: Int, contacts: Int), DataError>) -> Void)
}

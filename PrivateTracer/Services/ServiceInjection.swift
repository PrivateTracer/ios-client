//
//  ServiceInjection.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee.
//

import Foundation

/// Get the service you want to use with this enum. All the services are singletons.
enum ServiceInjection {

    static var dp3tService: DP3TService = DP3TServiceImplementation()
    static var seedRecordsService: SeedRecordsService = SeedRecordsServiceImplementation()
    static var apiService: ApiService = ApiServiceImplementation()
    static var infectionService: InfectionService = InfectionServiceImplementation()
    static var contactTracingService: ContactTracingService = ContactTracingServiceImplementation()
    static var contactRecordingService: ContactRecordingService = ContactRecordingServiceImplementation()
    static var bluetoothPeripheralService: BluetoothPeripheralService = BluetoothPeripheralServiceImpl()
    static var bluetoothCentralService: BluetoothCentralService = BluetoothCentralServiceImplementation()
    static var tracerTokenService: TracerTokenGenerator = TracerTokenGeneratorImplementation()
    static var dataManagementService: DataManagementService = DataManagementServiceImplementation()
}

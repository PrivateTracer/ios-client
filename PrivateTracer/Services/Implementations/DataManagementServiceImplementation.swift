//
//  DataManagementServiceImplementation.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 29/04/2020.
//

import Foundation

class DataManagementServiceImplementation: DataManagementService {
    
    func removeAllData(completion: @escaping (Result<(seeds: Int, contacts: Int), DataError>) -> Void) {
        let group = DispatchGroup()
        var errors = [DataError]()
        var seedRecords: Int = 0
        var contactRecords: Int = 0
        
        group.enter()
        ServiceInjection.seedRecordsService.removeAllData { result in
            switch result {
            case .success(let num):
                seedRecords = num
            case .failure(let error):
                errors.append(error)
            }
            group.leave()
        }
        
        group.enter()
        ServiceInjection.contactRecordingService.removeAllData { result in
            switch result {
            case .success(let num):
                contactRecords = num
            case .failure(let error):
                errors.append(error)
            }
            group.leave()
        }
        
        removeUserDefaults()
               
        group.notify(queue: DispatchQueue.global(qos: .background)) {
            if errors.count > 0 {
                var errorString = ""
                for error in errors {
                    switch error {
                    case .unknown:
                        errorString += "Unknown error"
                    case .system(let err):
                        errorString += err
                    }
                }
                DispatchQueue.main.async {
                    completion(.failure(DataError.system(localizedDescription: errorString)))
                }
            } else {
                DispatchQueue.main.async {
                    completion(.success((seeds: seedRecords, contacts: contactRecords)))
                }
            }
        }
        
    }
    
    private func removeUserDefaults() {
        ServiceInjection.dp3tService.resetAll()
        ServiceInjection.infectionService.resetAll()
    }
}

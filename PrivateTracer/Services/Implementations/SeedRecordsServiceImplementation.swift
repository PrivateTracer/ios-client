//
//  SeedRecordsServiceImplementation.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 17/04/2020.
//

import CoreData
import UIKit

class SeedRecordsServiceImplementation: SeedRecordsService {
    
    func seedHistory(completionBlock: @escaping (Result<[SeedRecord], DataError>) -> Void) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            completionBlock(.failure(.unknown))
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        managedContext.perform {
            let fetchRequest = NSFetchRequest<SeedRecord>(entityName: "SeedRecord")
            fetchRequest.fetchLimit = 100
            let sortDescriptor = NSSortDescriptor(key: SeedRecord.CodingKeys.epochId.rawValue, ascending: false)
            fetchRequest.sortDescriptors = [sortDescriptor]

            do {
                let seeds = try managedContext.fetch(fetchRequest)
                DispatchQueue.main.async {
                    completionBlock(.success(seeds))
                }
            } catch {
                LogHelper.debugLog("Could not fetch seeds data. \(error)")
                DispatchQueue.main.async {
                    completionBlock(.failure(.system(localizedDescription: error.localizedDescription)))
                }
            }
        }
    }
    
    func removeOldData(completionBlock: @escaping (Result<Int, DataError>) -> Void) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            completionBlock(.failure(.unknown))
            return
        }
        appDelegate.persistentContainer.performBackgroundTask { [weak self] managedContext in
            let fetchRequest = NSFetchRequest<SeedRecord>(entityName: "SeedRecord")
            fetchRequest.includesPropertyValues = false

            // For e.g. 31 March, we get reverseCutOffDate of 17 March
            let reverseCutOffDate: Date? = Calendar.current.date(byAdding: .day, value: -PrivateTracerConfig.TTLDays, to: Date())
            if let validDate = reverseCutOffDate {
                let predicateForDel = NSPredicate(format: "timestamp < %@", validDate as NSDate)
                fetchRequest.predicate = predicateForDel
                
                self?.deleteRecords(for: fetchRequest, withContext: managedContext, completion: completionBlock)
            }
        }
    }
    
    func removeAllData(completionBlock: @escaping (Result<Int, DataError>) -> Void) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            completionBlock(.failure(.unknown))
            return
        }
        appDelegate.persistentContainer.performBackgroundTask { [weak self] managedContext in
            let fetchRequest = NSFetchRequest<SeedRecord>(entityName: "SeedRecord")
            fetchRequest.includesPropertyValues = false

            self?.deleteRecords(for: fetchRequest, withContext: managedContext, completion: completionBlock)
        }

    }
    
    private func deleteRecords(for fetchRequest: NSFetchRequest<SeedRecord>, withContext context: NSManagedObjectContext, completion: @escaping (Result<Int, DataError>) -> Void) {
        do {
            let records = try context.fetch(fetchRequest)
            for record in records {
                context.delete(record)
            }
            if context.hasChanges {
                try context.save()
            }
            DispatchQueue.main.async {
                completion(.success(records.count))
            }
        } catch {
            LogHelper.debugLog("Could not perform delete of old data. \(error)")
            DispatchQueue.main.async {
                completion(.failure(.system(localizedDescription: error.localizedDescription)))
            }
        }
    }
}

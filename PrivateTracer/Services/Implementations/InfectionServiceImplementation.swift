//
//  InfectionServiceImplementation.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation

class InfectionServiceImplementation: InfectionService {

    // MARK: - Private members
    private let jsonEncoder = JSONEncoder()
    private let ud_InfectionState = "InfectionState"
    private let ud_LastTraceSetID = "LastTraceSetID"
    private let ud_LastTraceSetDate = "LastTraceSetDate"
    
    private var refreshTimer: Timer?
    private(set) var lastTraceSetID: LastTracesetFetch? {
        didSet {
            // Save
            UserDefaults.standard.set(lastTraceSetID?.lastID, forKey: ud_LastTraceSetID)
            UserDefaults.standard.set(lastTraceSetID?.dateFetched, forKey: ud_LastTraceSetDate)
            
            // Broadcast update
            lastUpdateCallback?(lastTraceSetID?.dateFetched)
            
            refreshTimer?.invalidate()
            refreshTimer = Timer.scheduledTimer(withTimeInterval: PrivateTracerConfig.TracesetUpdatePeriodInSeconds, repeats: true, block: { [weak self] _ in
                self?.checkForRiskyInteractions()
            })
        }
    }
    
    // MARK: - Public members
    var state: InfectionState = .clean {
        didSet {
            if let jsonData = try? jsonEncoder.encode(state) {
                UserDefaults.standard.setValue(jsonData, forKeyPath: ud_InfectionState)
            }
        }
    }
    
    var lastUpdateCallback: ((Date?) -> Void)?
    
    // MARK: - Public methods
    init() {
        if let jsonData = UserDefaults.standard.data(forKey: ud_InfectionState),
            let savedState = try? JSONDecoder().decode(InfectionState.self, from: jsonData) {
            updateInfectionState(savedState)
        } else {
            // App used for the first time, no saved state, so do nothing
        }
        
        // Check if we downloaded tracesets before
        let savedID = UserDefaults.standard.integer(forKey: ud_LastTraceSetID)
        if savedID != 0, let savedDate = UserDefaults.standard.object(forKey: ud_LastTraceSetDate) as? Date {
            lastTraceSetID = (savedDate, savedID)
        }
        
        if lastTraceSetID != nil {
            // Check for new risky interactions
            checkForRiskyInteractions()
        }
    }
    
    func setInfected(completionBlock: @escaping (Result<String, SubmitTraceSetFailure>) -> Void) {
        
        // Get current stored seeds
        ServiceInjection.seedRecordsService.seedHistory { [weak self] (result: Result<[SeedRecord], DataError>) in
            switch result {
            case .success(let seedsRecords):
                guard seedsRecords.isEmpty == false else {
                    completionBlock(.failure(.noTraces))
                    return
                }
                
                // Request confirmation code
                ServiceInjection.apiService.submitTraceSet(for: seedsRecords) { [weak self] result in
                    
                    if case Result<String, SubmitTraceSetFailure>.success = result {
                        self?.updateInfectionState(.selfInfected)
                    }
                    
                    completionBlock(result)
                }
            case .failure(let coreDataError):
                switch coreDataError {
                case .system(let error):
                    completionBlock(.failure(.system(localizedDescription: error)))
                case .unknown:
                    completionBlock(.failure(.unknown))
                }
            }
        }
        
    }
    
    func checkForRiskyInteractions(completionBlock: ((Result<Date?, TraceSetFailure>) -> Void)? = nil) {

        // Make sure we have the last Traceset ID
        guard let lastTraceSetID = lastTraceSetID else {
            fetchLastTraceSetID { [weak self] result in
                
                switch result {
                case .success:
                    // Try again!
                    self?.checkForRiskyInteractions(completionBlock: completionBlock)
                    
                case .failure(let failure):
                    switch failure {
                    case .noInternet: completionBlock?(.failure(.noInternet))
                    case .noTraceSets: completionBlock?(.failure(.notFound))
                    case .system(let localizedDescription): completionBlock?(.failure(.system(localizedDescription: localizedDescription)))
                    case .timeout: completionBlock?(.failure(.timeout))
                    case .unknown: completionBlock?(.failure(.unknown))
                    }
                }
            }
            return
        }
        
        // TODO: change to fetch the current latest traceset first, don't auto-increment.
        // Get the next traceset
        let nextTracesetID = lastTraceSetID.lastID + 1
        LogHelper.debugLog("[INFECTION] Fetching traceset \(nextTracesetID)")
        ServiceInjection.apiService.traceset(nextTracesetID) { [weak self] result in
            
            switch result {
            case .success(let data):
                
                // Process trace set
                self?.matchNewTracingset(data)
                
                // Save this trace set ID
                self?.lastTraceSetID = (Date(), nextTracesetID)
                
                // Try next traceset, always make sure lastTracesetID was incremented,
                // to prevent infinite loop!
//                self?.checkForRiskyInteractions(completionBlock: completionBlock)
                
            case .failure(let failure):
                
                if case TraceSetFailure.notFound = failure {
                    // We've reached the end of the loop! No more tracesets.
                    LogHelper.debugLog("[INFECTION] Traceset not found, end of loop")
                                        
                    // Save new check date with last ID
                    self?.lastTraceSetID = (Date(), lastTraceSetID.lastID)
                    
                    completionBlock?(.success(lastTraceSetID.dateFetched))
                } else {
                    completionBlock?(.failure(failure))
                }
            }
        }
    }
    
    func resetAll() {
        UserDefaults.standard.removeObject(forKey: ud_InfectionState)
        UserDefaults.standard.removeObject(forKey: ud_LastTraceSetID)
        UserDefaults.standard.removeObject(forKey: ud_LastTraceSetDate)
        state = .clean
        refreshTimer?.invalidate()
    }
    
    // MARK: - Private methods
    private func matchNewTracingset(_ tracingSet: Data) {
        ServiceInjection.contactRecordingService.recordedContacts {[weak self] result in
            switch result {
            case .success(let contacts):
                // Match traceset and contacts
                if let mostRecentRiskyInteractionDate = ServiceInjection.dp3tService.checkForRiskyInteractions(contacts: contacts, tracesetJsonData: tracingSet) {
                    // We've had a recent risky interaction!
                    LogHelper.debugLog("[INFECTION] New risky interaction on \(mostRecentRiskyInteractionDate.debugDescription)")
                    self?.updateInfectionState(.riskyInteractions(lastRiskyInteraction: mostRecentRiskyInteractionDate))
                } else {
                    LogHelper.debugLog("[INFECTION] No risky interaction in traceset")
                }
            case .failure(let error):
                LogHelper.debugLog("[INFECTION] Error when fetching contacts: \(error)")
            }
        }
    }
    
    private func fetchLastTraceSetID(completionBlock: ((Result<LastTracesetFetch, LastTraceSetIDFailure>) -> Void)? = nil) {
        
        LogHelper.debugLog("[INFECTION] Fetching initial traceset ID")
        
        ServiceInjection.apiService.traceSetCount { [weak self] result in
            switch result {
            case .success(let lastID):
                
                LogHelper.debugLog("[INFECTION] Initial traceset ID = \(lastID)")
                
                // We need to go -1, because we fetch the _next_
                let previousLastID = max(0, lastID - 1)
                let lastTraceSetID = (Date(), previousLastID)
                self?.lastTraceSetID = lastTraceSetID
                completionBlock?(.success(lastTraceSetID))
                
            case .failure(let reason):
                completionBlock?(.failure(reason))
            }
        }
    }
    
    private func updateInfectionState(_ possibleNewState: InfectionState) {
        
        // Determine whether this new state is more important
        guard state < possibleNewState else { return }

        state = possibleNewState
    }
}

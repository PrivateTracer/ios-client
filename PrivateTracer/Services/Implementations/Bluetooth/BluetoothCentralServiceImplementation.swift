//
//  BluetoothCentralServiceImplementation.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 18/04/2020.
//

import Foundation
import CoreBluetooth

class BluetoothCentralServiceImplementation: NSObject, BluetoothCentralService {
    
    private let restoreIdentifierKey = "org.privatetracker.central"
    private var queue: DispatchQueue
    private var central: CBCentralManager?
    private var epochId: NSNumber?
    private var observationManager: ObservationManager?
    /// Peripherals get stored during one epoch cycle so we can be a delegate for them
    private var storedPeripherals: Set<CBPeripheral>?
    /// The peripherals we already saw in the current scan cycle
    private var scannedPeripherals: [UUID]?
    
    private var timerForScanning: Timer?
    
    override init() {
        queue = DispatchQueue(label: "BluetoothCentralService")
        super.init()
    }
    
    func start(withObservationManager manager: ObservationManager) {
        guard central == nil else {
            if central!.isScanning {
                return
            }
            storedPeripherals = Set<CBPeripheral>()
            startScanning(central!)
            return
        }
        self.observationManager = manager
        storedPeripherals = Set<CBPeripheral>()
        scannedPeripherals = [UUID]()
        central = CBCentralManager(delegate: self, queue: self.queue, options: [CBCentralManagerOptionRestoreIdentifierKey: restoreIdentifierKey])
    }
    
    func stop() {
        timerForScanning?.invalidate()
        timerForScanning = nil
        guard central != nil else {
            return
        }
        central?.stopScan()
        // For all peripherals that are not disconnected, disconnect them
        disconnectPeripherals()
        storedPeripherals = nil
        scannedPeripherals = nil
    }

    func disconnectPeripherals() {
        storedPeripherals?.forEach { peripheral in
            central?.cancelPeripheralConnection(peripheral)
        }
    }
}

extension BluetoothCentralServiceImplementation: CBCentralManagerDelegate {
    
    func centralManager(_ central: CBCentralManager, willRestoreState dict: [String: Any]) {
        LogHelper.debugLog("[BTCentralService] centralManager willRestoreState")
    }
    
    func startScanning(_ central: CBCentralManager) {
        DispatchQueue.main.async {
            self.timerForScanning?.invalidate()
            self.timerForScanning = Timer.scheduledTimer(withTimeInterval: TimeInterval(PrivateTracerConfig.CentralScanInterval), repeats: true) { _ in
                LogHelper.debugLog("")

                // For all peripherals that are not disconnected, disconnect them
                self.disconnectPeripherals()

                // Check only for peripherals adverstising our service
                
                central.scanForPeripherals(withServices: [PrivateTracerConfig.BluetoothServiceID])
                LogHelper.debugLog("[BTCentralService] start scan")

                DispatchQueue.global().asyncAfter(deadline: .now() + .seconds(PrivateTracerConfig.CentralScanDuration)) { [weak central, weak self] in
                    LogHelper.debugLog("[BTCentralService] stop scan")
                    central?.stopScan()
                    self?.scannedPeripherals = [UUID]()
                }
            }
            self.timerForScanning?.fire()
        }
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        LogHelper.debugLog("[BTCentralService] centralManager didUpdateState")
        switch central.state {
        case .poweredOn:
            startScanning(central)
        default:
            timerForScanning?.invalidate()
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String: Any], rssi RSSI: NSNumber) {

        LogHelper.debugLog("[BTCentralService] discovered peripheral with uuid: \(peripheral.identifier)")
        
        if let peripherals = scannedPeripherals,
            peripherals.contains(peripheral.identifier) {
            LogHelper.debugLog("[BTCentralService] Ignoring peripheral with uuid: \(peripheral.identifier) since we already saw it in this scan")
            return
        } else {
            scannedPeripherals?.append(peripheral.identifier)
        }

        // Try to get the ephemeral ID directly, will work for Android
        var ephID: UUID?
        if let ephIdDataPair = advertisementData[CBAdvertisementDataServiceDataKey] as? [CBUUID: Data],
            let ephemeralID = ephIdDataPair[PrivateTracerConfig.ShortBluetoothServiceID] {
            ephID = Array(ephemeralID).uuid
        }

        // iOS peripherals: connect to the peripheral to fetch the ephID
        if ephID == nil && self.observationManager?.hasObservationWithUuid(peripheral.identifier) == false {
            // We haven't seen this one yet so we need to connect
            peripheral.delegate = self
            central.connect(peripheral)
        }

        // Save the initial observation, even if we don't have the ephId yet, will be matched to a later observation with ephId
        let txLevel = advertisementData[CBAdvertisementDataTxPowerLevelKey] as? NSNumber
        
        let observation = Observation(
            ephId: ephID,
            timestamp: Date(),
            txLevel: txLevel,
            rssi: RSSI
        )
        self.observationManager?.addObservationWithUuid(
            peripheral.identifier,
            values: observation)
        self.storedPeripherals?.insert(peripheral)
    }

    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.delegate = self
        peripheral.discoverServices([PrivateTracerConfig.BluetoothServiceID])
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        // Todo: what happens here?
        LogHelper.debugLog("[BTCentralService] Peripheral with UUID \(peripheral.identifier) got disconnected")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error _: Error?) {
        LogHelper.debugLog("[BTCentralService] Measured an RSSI of \(RSSI) for peripheral with UUID \(peripheral.identifier)")
        // Need to decide if we want to do something with this value. For now, these observations get discarded.
        // let observation = Observation(ephId: nil, timestamp: Date(), txLevel: nil, rssi: RSSI)
        // self.observationManager?.addObservationWithUuid(peripheral.identifier, peripheral: peripheral, values: observation)
    }
    
}

extension BluetoothCentralServiceImplementation: CBPeripheralDelegate {
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if let err = error {
            LogHelper.debugLog("[BTCentralService] Error: \(err)")
            central?.cancelPeripheralConnection(peripheral)
        }
        guard let service = peripheral.services?.first(where: { $0.uuid == PrivateTracerConfig.BluetoothServiceID }) else {
            central?.cancelPeripheralConnection(peripheral)
            return
        }

        peripheral.discoverCharacteristics(nil, for: service)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if let err = error {
            LogHelper.debugLog("[BTCentralService] Error: \(err)")
        }
        guard let characteristic = service.characteristics?.first else {
            central?.cancelPeripheralConnection(peripheral)
            return
        }
        central?.cancelPeripheralConnection(peripheral)
        LogHelper.debugLog("[BTCentralService] Connected to peripheral with uuid: \(peripheral.identifier), found ephID: \(characteristic.uuid)")
        let observation = Observation(fromCBUUID: characteristic.uuid, timestamp: Date())
        self.observationManager?.addObservationWithUuid(
            peripheral.identifier,
            values: observation)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
        guard let service = invalidatedServices.first(where: { $0.uuid == PrivateTracerConfig.BluetoothServiceID }) else { return }
        peripheral.discoverCharacteristics(nil, for: service)
    }
}

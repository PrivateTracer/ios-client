//
//  ContactTracingService.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 18/04/2020.
//

import Foundation
import CoreBluetooth

class ContactTracingServiceImplementation: ContactTracingService {
    
    typealias Observations = [UUID: [Observation]]
    typealias CachedEphIDs = [UUID: UUID]
    typealias CachedObservations = [UUID: (rssi: NSNumber, txLevel: NSNumber?)]
    
    /// Observations keyed by peripheral UUID
    internal var observations = Observations()
    /// Observations that don't have an ephID (yet)
    internal var cachedObservations = CachedObservations()
    /// ephIDs we've seen during the current epoch cycle
    internal var cachedEphIDs = CachedEphIDs()

    private var epochId: Int32?
    private var dp3tService = ServiceInjection.dp3tService
    
    init() {
        epochId = dp3tService.epochAndSeed?.id
    }
    
    func start() {
        ServiceInjection.infectionService.checkForRiskyInteractions(completionBlock: nil)
        dp3tService.start { [weak self] ephemeralID in
            self?.resetEpochWithEphID(ephemeralID)
        }
    }
    
    func stop() {
        ServiceInjection.bluetoothPeripheralService.stop()
        ServiceInjection.bluetoothCentralService.stop()
        saveAndFlushObservations()
        dp3tService.stop()
    }
    
    private func resetObservations() {
        observations = Observations()
        cachedObservations = CachedObservations()
        cachedEphIDs = CachedEphIDs()
    }
    
    private func resetEpochWithEphID(_ ephID: UUID) {
        epochId = ServiceInjection.dp3tService.epochAndSeed?.id
        saveAndFlushObservations()
        ServiceInjection.bluetoothPeripheralService.startWithEphID(ephID)
        ServiceInjection.bluetoothCentralService.start(withObservationManager: self)
    }
    
    private func saveAndFlushObservations() {
        for observation in self.observations {
            let firstTimestamp = observation.value.reduce(Date()) {
                min($0, $1.timestamp)
            }
            let lastTimestamp = observation.value.reduce(Date()) {
                max($0, $1.timestamp)
            }
            let riskyObservations = observation.value.filter({
                if let rssi = $0.rssi as? Double {
                    if rssi <= PrivateTracerConfig.RiskyContactMinimumRssi {
                        LogHelper.debugLog("[ContactTracingService] Discarded observation with RSSI: \(rssi)")
                    }
                    return rssi > Double(PrivateTracerConfig.RiskyContactMinimumRssi)
                }
                return false
            })
            let minNumberOfObservations = Int(PrivateTracerConfig.RiskyContactMinimumDuration / Double(PrivateTracerConfig.CentralScanDuration))
            
            if lastTimestamp.timeIntervalSince(firstTimestamp) >= PrivateTracerConfig.RiskyContactMinimumDuration
            && riskyObservations.count >= minNumberOfObservations {
                if let contactObservation = riskyObservations.last {
                    LogHelper.debugLog("[ContactTracingService] Saving a contact")
                    saveContact(observation: contactObservation)
                } else {
                    LogHelper.debugLog("[ContactTracingService] Not saving contact????: min observations: \(minNumberOfObservations), observations count: \(riskyObservations.count)" )
                }
            } else {
                LogHelper.debugLog("[ContactTracingService] No observations or not enough observations for peripheral with UUID: \(observation.key)")
            }
        }
        resetObservations()
    }
    
    private func saveContact(observation: Observation) {
        guard let ephId = observation.ephId, let epoch = epochId else {
            LogHelper.debugLog("[ContactTracingService] WARNING: Tried saving contact without ephId or without epoch.")
            return
        }
        ServiceInjection.contactRecordingService.recordContact(withEphID: ephId, timestamp: observation.timestamp, epochID: epoch) {_ in }
    }
    
}

extension ContactTracingServiceImplementation: ObservationManager {

    func addObservationWithUuid(_ uuid: UUID, values: Observation) {
        // We check if the observation has an EphId, if not it gets added to the cache
        // Observations with EphId get their rssi and txLevel from the cache, if they don't have it already
        if let ephId = values.ephId {
            var newObservation: Observation
            if cachedObservations.keys.contains(uuid) && values.rssi == nil {
                // Get the RSSI from a previous cached observation
                newObservation = Observation(ephId: ephId, timestamp: values.timestamp, txLevel: cachedObservations[uuid]!.txLevel, rssi: cachedObservations[uuid]!.rssi)
                cachedObservations.removeValue(forKey: uuid)
            } else {
                guard values.rssi != nil else {
                    LogHelper.debugLog("[ContactTracingService] Received an observation with ephID: \(String(describing: values.ephId)) without power levels that we cannot match to a previous observation")
                    return
                }
                // Observation contains everything we need
                newObservation = values
            }
            LogHelper.debugLog("[ContactTracingService] Observation recorded with ephID: \(ephId), RSSI: \(newObservation.rssi!)")
            // Check if uuid has changed before recording the observation
            if let previousUuid = cachedEphIDs[ephId],
                   previousUuid != uuid,
                   let previousObservations = observations[previousUuid] {
                LogHelper.debugLog("[ContactTracingService] Switch to new peripheral UUID for ephID: \(ephId)")
                    observations[uuid] = previousObservations
                    observations.removeValue(forKey: previousUuid)
            }
            if observations.keys.contains(uuid) {
                observations[uuid]!.append(newObservation)
            } else {
                observations[uuid] = [newObservation]
            }
            cachedEphIDs[ephId] = uuid
        } else {
            guard values.rssi != nil else {
                LogHelper.debugLog("[ContactTracingService] Received an observation for UUID: \(uuid) without power levels that doesn't have an ephID")
                return
            }
            // Check if there's already an observation with EphId for this peripheral, in which case this is a new observation
            if let previousObservation = observations[uuid] {
                // Associate the ephID with the new observation
                let ephId = previousObservation.first?.ephId
                let newObservation = Observation(ephId: ephId, timestamp: values.timestamp, txLevel: values.txLevel, rssi: values.rssi)
                observations[uuid]?.append(newObservation)
                LogHelper.debugLog("[ContactTracingService] Observation recorded with ephID: \(newObservation.ephId!), RSSI: \(newObservation.rssi!)")
            } else {
                // Keep this one in cache for now, until we get the EphID
                cachedObservations[uuid] = (rssi: values.rssi!, txLevel: values.txLevel)
                LogHelper.debugLog("[ContactTracingService] First observation without ephId for peripheral with UUID: \(uuid)")
            }
        }
    }
        
    func hasObservationWithUuid(_ uuid: UUID) -> Bool {
        return observations.keys.contains(uuid)
    }
}

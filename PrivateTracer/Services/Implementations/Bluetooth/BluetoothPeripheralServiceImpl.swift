//
//  BluetoothPeripheralServiceImpl.swift
//  PrivateTracer
//
//  Created by Sumeru Chatterjee on 17/04/2020.
//

import Foundation
import CoreBluetooth

class BluetoothPeripheralServiceImpl: NSObject, BluetoothPeripheralService {
    
    private var peripheral: CBPeripheralManager!
    private var service: CBMutableService?
    private var queue: DispatchQueue
    private var peripheralName = "PrivateTraceriOS"
    private let restoreIdentifierKey = "org.privatetracker.peripheral"
    private var readableCharacteristic: CBMutableCharacteristic?
    private var ephemeralID: UUID? {
        didSet {
            if ephemeralID != nil && ephemeralID != oldValue {
                readableCharacteristic = CBMutableCharacteristic(type: CBUUID(nsuuid: ephemeralID!), properties: [.read, .notify], value: nil, permissions: [.readable])
                startBroadcasting()
            }
        }
    }
        
    override init() {
        queue = DispatchQueue(label: "BluetoothPeripheralService")
        super.init()
    }
        
    func startWithEphID(_ ephID: UUID) {
        ephemeralID = ephID
    }
    
    func startBroadcasting() {
        stopBroadcasting()
        // Need to wait a bit, otherwise the state of the peripheral gets stuck on .unsupported
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) { [weak self] in
            guard let `self` = self else { return }
            LogHelper.debugLog("[BTPeripheralService] Broadcast ephID: \(self.ephemeralID!)")
            self.peripheral = CBPeripheralManager(delegate: self, queue: self.queue, options: [
                CBPeripheralManagerOptionShowPowerAlertKey: NSNumber(value: true),
                CBPeripheralManagerOptionRestoreIdentifierKey: self.restoreIdentifierKey])
        }
    }
    
    func stop() {
        stopBroadcasting()
    }
    
    func stopBroadcasting() {
        guard peripheral != nil else {
            return
        }
        peripheral.stopAdvertising()
        peripheral.removeAllServices()
        peripheral = nil
    }
}

extension BluetoothPeripheralServiceImpl: CBPeripheralManagerDelegate {
    
    public func peripheralManager(_ peripheral: CBPeripheralManager, willRestoreState dict: [String: Any]) {
        LogHelper.debugLog("[BTPeripheralService] peripheralManager willRestoreState")
        if let services: [CBMutableService] = dict[CBPeripheralManagerRestoredStateServicesKey] as? [CBMutableService],
            let service = services.first(where: { $0.uuid == PrivateTracerConfig.BluetoothServiceID }) {
            self.service = service
        }
    }

    public func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        guard peripheral.state == .poweredOn else {
            LogHelper.debugLog("[BTPeripheralService] peripheralManager state is not poweredOn: \(peripheral.state.rawValue)")
            return
        }
        
        guard readableCharacteristic != nil else { return }
        
        LogHelper.debugLog("[BTPeripheralService] peripheralManager start advertising")
        
        let tracerService = CBMutableService(type: PrivateTracerConfig.BluetoothServiceID, primary: true)
        tracerService.characteristics = [readableCharacteristic!]
        peripheral.removeAllServices()
        peripheral.add(tracerService)
        service = tracerService
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didAdd service: CBService, error _: Error?) {
        let advertisementData: [String: Any] = [CBAdvertisementDataLocalNameKey: peripheralName,
                                                CBAdvertisementDataServiceUUIDsKey: [PrivateTracerConfig.BluetoothServiceID]]
        peripheral.startAdvertising(advertisementData)
    }
    
    public func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveRead request: CBATTRequest) {
        LogHelper.debugLog("[BTPeripheralService] peripheralManager didReceiveRead")
        
        let dummy = "Look at the characteristic UUID"
        request.value = dummy.data(using: .utf8)
        peripheral.respond(to: request, withResult: .success)
    }

    public func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveWrite requests: [CBATTRequest]) {
        LogHelper.debugLog("[BTPeripheralService] peripheralManager didReceiveWrite")
    }
}

//
//  TracerTokenServiceImplementation.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 24/04/2020.
//

import Foundation

class TracerTokenGeneratorImplementation: TracerTokenGenerator {
    
    private let VALID_CHARS = Array("ABCDEFGHJKLMNPRSTUVWXYZ23456789")
    
    func newTracerToken() -> String {
        // Note `randomElement()` in swift on iOS uses the cryptographically secure `arc4random_buf(3)`, see https://developer.apple.com/documentation/swift/systemrandomnumbergenerator
        let randomArray = (0..<9).map { _ in VALID_CHARS.randomElement()! }
        var randomCharacters = String(randomArray)
        // Add checksum
        // Note: It's ok to force unwrap here because we're using the correct input chars.
        randomCharacters.append(generateCheckCharacter(from: randomCharacters)!)
        return randomCharacters.withDashesAdded()
    }
    
    func isValidTracerToken(_ token: String) -> Bool {
        let tokenString = token.withDashesRemoved()
        guard let remainder = remainder(from: tokenString, startingFactor: 1) else {
            return false
        }
        return remainder == 0
    }
    
    private func generateCheckCharacter(from token: String) -> Character? {
        guard let remainder = remainder(from: token, startingFactor: 2) else {
            return nil
        }
        let n = VALID_CHARS.count
        let checkCodePoint = (n - remainder) % n
        return character(from: checkCodePoint)
    }
    
    private func remainder(from token: String, startingFactor f: Int) -> Int? {
        var factor = f
        var sum = 0
        let n = VALID_CHARS.count

        // Starting from the right, work leftwards
        // Now, the initial "factor" will always be "1"
        // since the last character is the check character.
        for char in token.reversed() {
            guard let codePoint = codePoint(from: char) else {
                return nil
            }
            var addend = factor * codePoint
            
            // Alternate the "factor" that each "codePoint" is multiplied by
            factor = (factor == 2) ? 1 : 2

            // Sum the digits of the "addend" as expressed in base "n"
            addend = Int(addend / n) + (addend % n)
            sum += addend
        }
        
        let remainder = sum % n
        return remainder
    }
    
    private func codePoint(from char: Character) -> Int? {
        return VALID_CHARS.firstIndex(of: char)
    }

    private func character(from codePoint: Int) -> Character {
        return VALID_CHARS[codePoint]
    }

}

extension String {
    func withDashesAdded() -> String {
        return self.inserting(separator: "-", every: 3, minLength: 2)
    }

    func withDashesRemoved() -> String {
        return self.replacingOccurrences(of: "-", with: "")
    }
}

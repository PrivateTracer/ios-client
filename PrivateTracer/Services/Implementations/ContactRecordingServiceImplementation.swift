//
//  ContactRecordingServiceImplementation.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 19/04/2020.
//

import Foundation
import UIKit
import CoreData

class ContactRecordingServiceImplementation: ContactRecordingService {
    
    func recordContact(withEphID ephID: UUID, timestamp: Date, epochID: Epoch, completion: @escaping (Result<Void, DataError>) -> Void) {
        let identity = ServiceInjection.dp3tService.generateHashedIdentityBase64(ephemeralIdentifier: ephID, epochIdentifier: epochID)
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                completion(.failure(.unknown))
                return
        }
        appDelegate.persistentContainer.performBackgroundTask { managedContext in
            let entity = NSEntityDescription.entity(forEntityName: "ContactRecord", in: managedContext)!
            let contactRecord = ContactRecord(entity: entity, insertInto: managedContext)
            contactRecord.setValuesForKeys(["identity": identity, "timestamp": timestamp])
            do {
                try managedContext.save()
                DispatchQueue.main.async {
                    completion(.success(()))
                }
            } catch let error as NSError {
                LogHelper.debugLog("[ContactRecordingService] Could not save. \(error), \(error.userInfo)")
                DispatchQueue.main.async {
                    completion(.failure(.system(localizedDescription: error.localizedDescription)))
                }
            }
        }
    }
    
    func recordedContacts(completion: @escaping (Result<[ContactRecord], DataError>) -> Void) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            completion(.failure(.unknown))
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        managedContext.perform {
            let fetchRequest = NSFetchRequest<ContactRecord>(entityName: "ContactRecord")
            fetchRequest.fetchLimit = 100
            let sortDescriptor = NSSortDescriptor(key: ContactRecord.CodingKeys.timestamp.rawValue, ascending: false)
            fetchRequest.sortDescriptors = [sortDescriptor]

            do {
                let contacts = try managedContext.fetch(fetchRequest)
                DispatchQueue.main.async {
                    completion(.success(contacts))
                }
            } catch {
                LogHelper.debugLog("[ContactRecordingService] Could not fetch contact data. \(error)")
                DispatchQueue.main.async {
                    completion(.failure(.system(localizedDescription: error.localizedDescription)))
                }
            }
        }
    }
    
    func removeOldData(completion: @escaping (Result<Int, DataError>) -> Void) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            completion(.failure(.unknown))
            return
        }
        appDelegate.persistentContainer.performBackgroundTask { [weak self] managedContext in
            let fetchRequest = NSFetchRequest<ContactRecord>(entityName: "ContactRecord")
            fetchRequest.includesPropertyValues = false

            // For e.g. 31 March, we get reverseCutOffDate of 17 March
            let reverseCutOffDate: Date? = Calendar.current.date(byAdding: .day, value: -PrivateTracerConfig.TTLDays, to: Date())
            if let validDate = reverseCutOffDate {
                let predicateForDel = NSPredicate(format: "timestamp < %@", validDate as NSDate)
                fetchRequest.predicate = predicateForDel
                self?.deleteRecords(for: fetchRequest, withContext: managedContext, completion: completion)
            }
        }
    }
    
    func removeAllData(completion: @escaping (Result<Int, DataError>) -> Void) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            completion(.failure(.unknown))
            return
        }
        appDelegate.persistentContainer.performBackgroundTask { [weak self] managedContext in
            let fetchRequest = NSFetchRequest<ContactRecord>(entityName: "ContactRecord")
            fetchRequest.includesPropertyValues = false
            self?.deleteRecords(for: fetchRequest, withContext: managedContext, completion: completion)
        }
    }
    
    private func deleteRecords(for fetchRequest: NSFetchRequest<ContactRecord>, withContext context: NSManagedObjectContext, completion: @escaping (Result<Int, DataError>) -> Void) {
        do {
            let records = try context.fetch(fetchRequest)
            for record in records {
                context.delete(record)
            }
            if context.hasChanges {
                try context.save()
            }
            DispatchQueue.main.async {
                completion(.success(records.count))
            }
        } catch {
            LogHelper.debugLog("Could not perform delete of old data. \(error)")
            DispatchQueue.main.async {
                completion(.failure(.system(localizedDescription: error.localizedDescription)))
            }
        }
    }

}

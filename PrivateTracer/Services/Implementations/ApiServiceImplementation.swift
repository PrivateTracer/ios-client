//
//  ApiServiceImplementation.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation

class ApiServiceImplementation: ApiService {
    private let baseURL = PrivateTracerConfig.BaseURL
    private let jsonEncoder: JSONEncoder = {
        let encoder = JSONEncoder()
        
        if #available(iOS 13.0, *) {
            // Disable slash escaping on iOS 13
            encoder.outputFormatting = .withoutEscapingSlashes
        }
        
        return encoder
    }()
            
    func traceSetCount(completionBlock innerCompletionBlock: @escaping (Result<Int, LastTraceSetIDFailure>) -> Void) -> URLSessionDataTask? {
                
        let mainThreadCompletionBlock = { (result: Result<Int, LastTraceSetIDFailure>) in
            DispatchQueue.main.async {
                innerCompletionBlock(result)
            }
        }
        
        let request = getRequest(path: "tracingsets", method: .get)
        return doRequest(for: request) { (result: Result<Data?, ApiFailure>) in
            
            switch result {
            case .success(let data):
                
                guard
                    let data = data,
                    let countString = String(data: data, encoding: .utf8),
                    let count = Int(countString)
                    else {
                        LogHelper.debugLog("[API] Error: traceset count body was invalid")
                        mainThreadCompletionBlock(.failure(.unknown))
                        return
                }
                
                guard count > 0 else {
                    LogHelper.debugLog("[API] Error: Tracesets count <= 0")
                    mainThreadCompletionBlock(.failure(.noTraceSets))
                    return
                }
                                
                mainThreadCompletionBlock(.success(count))
                
            case .failure(let failure):
                switch failure {
                case .httpStatusCode:
                    mainThreadCompletionBlock(.failure(.unknown))
                    
                case .noInternet:
                     mainThreadCompletionBlock(.failure(.noInternet))
                    
                case .system(let localizedDescription):
                     mainThreadCompletionBlock(.failure(.system(localizedDescription: localizedDescription)))
                    
                case .timeout:
                     mainThreadCompletionBlock(.failure(.timeout))
                    
                case .unknown:
                     mainThreadCompletionBlock(.failure(.unknown))
                }
            }
        }
    }
    
    func traceset(_ id: Int, completionBlock innerCompletionBlock: @escaping (Result<Data, TraceSetFailure>) -> Void) -> URLSessionDataTask? {
        
        let mainThreadCompletionBlock = { (result: Result<Data, TraceSetFailure>) in
            DispatchQueue.main.async {
                innerCompletionBlock(result)
            }
        }
        
        let request = getRequest(path: "tracingsets/\(String(id))", method: .get)
        return doRequest(for: request) { (result: Result<Data?, ApiFailure>) in
                        
            switch result {
            case .success(let data):
                
                guard let data = data else {
                    LogHelper.debugLog("[API] Error: traceset body empty")
                    mainThreadCompletionBlock(.failure(.unknown))
                    return
                }
                
                mainThreadCompletionBlock(.success(data))
                
            case .failure(let failure):
                switch failure {
                case .httpStatusCode(let code):
                    
                    // TODO: Backend fixes 400 -> 404, or we have to fetch last ID first
                    // and then loop to that, instead of waiting for 404.
                    if code == 404 || code == 400 {
                        mainThreadCompletionBlock(.failure(.notFound)) // Reached the end
                    } else {
                        mainThreadCompletionBlock(.failure(.unknown))
                    }
                    
                case .noInternet:
                     mainThreadCompletionBlock(.failure(.noInternet))
                    
                case .system(let localizedDescription):
                     mainThreadCompletionBlock(.failure(.system(localizedDescription: localizedDescription)))
                    
                case .timeout:
                     mainThreadCompletionBlock(.failure(.timeout))
                    
                case .unknown:
                     mainThreadCompletionBlock(.failure(.unknown))
                }
            }
        }
    }
    
    @discardableResult func submitTraceSet(for infectedSeeds: [SeedRecord], completionBlock: @escaping (Result<String, SubmitTraceSetFailure>) -> Void) -> URLSessionDataTask? {
        
        var request = getRequest(path: "tracer", method: .post)
                
        // Setup post body
        let token = ServiceInjection.tracerTokenService.newTracerToken()
        let tracer = ApiTracer(with: token, and: infectedSeeds)
        guard let postBody = try? jsonEncoder.encode(tracer) else {
            completionBlock(.failure(.unknown))
            return nil
        }
        request.httpBody = postBody
        
        #if DEBUG
        LogHelper.debugLog("[API] URL: \(request.url!.absoluteString)")
        request.allHTTPHeaderFields?.forEach { (keyValue) in
            LogHelper.debugLog("[API] Header: [\"\(keyValue.key)\":\"\(keyValue.value)\"]")
        }
        LogHelper.debugLog("[API] PostBody: \(String(data: postBody, encoding: .utf8)!)")
        #endif
        
        // Do actual request
        return doRequest(for: request) { [weak self] (result: Result<Void, ApiFailure>) in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    completionBlock(.success(token))
                case .failure(let apiFailure):
                    
                    switch apiFailure {
                    case .httpStatusCode(let code):
                        
                        if code == 409 {
                            // Duplicate token, try again!
                            self?.submitTraceSet(for: infectedSeeds, completionBlock: completionBlock)
                        } else {
                            completionBlock(.failure(.unknown))
                        }
                        
                    case .noInternet:
                         completionBlock(.failure(.noInternet))
                        
                    case .system(let localizedDescription):
                         completionBlock(.failure(.system(localizedDescription: localizedDescription)))
                        
                    case .timeout:
                         completionBlock(.failure(.timeout))
                        
                    case .unknown:
                         completionBlock(.failure(.unknown))
                    }
                }
            }
        }
    }
    
    // MARK: - doRequest convience methods
    
    enum ApiFailure: Error {
        case noInternet
        case timeout
        case system(localizedDescription: String)
        case httpStatusCode(code: Int)
        case unknown
    }
    
    /// Do a request and returns a JSON decoded object
    /// - Parameters:
    ///   - request: request to perform
    ///   - completionBlock: Completion block with result with failure or decoded object
    /// - Returns: Data task so you can cancel if necessary
    @discardableResult private func doRequest<T: Decodable>(for request: URLRequest, completionBlock: @escaping (Result<T, ApiFailure>) -> Void) -> URLSessionDataTask? {
        
        return doRequest(for: request) { (result: Result<Data?, ApiFailure>) in

            guard case Result<Data?, ApiFailure>.success(let optionalData) = result,
                  let data = optionalData else {
                return completionBlock(.failure(.unknown)) // We expected data to decode
            }
            
            let resultObject: T
            do {
                resultObject = try JSONDecoder().decode(T.self, from: data)
            } catch {
                return completionBlock(.failure(.system(localizedDescription: error.localizedDescription)))
            }
            
            completionBlock(.success(resultObject))
        }
    }
    
    /// Do a request expecting no errors and a HTTP 200 OK
    /// - Parameters:
    ///   - request: request to perform
    ///   - completionBlock: Completion block with result with failure or void when success
    /// - Returns: Data task so you can cancel if necessary
    @discardableResult private func doRequest(for request: URLRequest, completionBlock: @escaping (Result<Void, ApiFailure>) -> Void) -> URLSessionDataTask? {
        return doRequest(for: request) { (result: Result<Data?, ApiFailure>) in
            switch result {
            case .success: completionBlock(.success(()))
            case .failure(let failure): completionBlock(.failure(failure))
            }
        }
    }
    
    /// Do a request expecting no errors, returns data for when you want something with it
    /// - Parameters:
    ///   - request: request to perform
    ///   - completionBlock: Completion block with result with failure or Data? when success
    /// - Returns: Data task so you can cancel if necessary
    @discardableResult private func doRequest(for request: URLRequest, completionBlock: @escaping (Result<Data?, ApiFailure>) -> Void) -> URLSessionDataTask? {
                
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                return completionBlock(.failure(.system(localizedDescription: error.localizedDescription)))
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                guard httpResponse.statusCode >= 200 && httpResponse.statusCode < 300 else {
                    return completionBlock(.failure(.httpStatusCode(code: httpResponse.statusCode)))
                }
            }
                        
            completionBlock(.success(data))
        }
        
        task.resume()
        return task
    }
    
    // MARK: - Request creation helper
    private enum HTTPMethod: String {
        case get = "GET"
        case post = "POST"
        case put = "PUT"
        case delete = "DELETE"
    }
    
    private func getRequest(path: String, method: HTTPMethod = .get) -> URLRequest {
        var request = URLRequest(url: URL(string: path, relativeTo: baseURL)!)
        request.httpMethod = method.rawValue
        
        if method == .post || method == .put {
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        
        return request
    }
}

//
//  DP3TServiceImplementation.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation

/// DP3TService wraps the actual C library.
/// Work in progress following https://gitlab.com/PrivateTracer/android-dp3t-library/-/blob/master/README.md
class DP3TServiceImplementation: DP3TService {
    
    // MARK: - Private members
    private let jsonEncoder = JSONEncoder()
    private let ud_Epoch = "CurrentEpoch"
    private var epochTimer: Timer?
    private var isStarted: Bool { epochChangeCallback != nil }
    
    private var epochChangeCallback: ((UUID) -> Void)? {
        didSet {
            // Check for refresh
            if checkEpochAndSeedExpiration() == false {
                // Immediatly notify new subscriber
                broadCastEphemeralIdentifier()
            }
        }
    }
    
    // MARK: - Public members
    private(set) var epochAndSeed: DP3TEpochAndSeed? {
        didSet {
                        
            // Refresh when epoch expires
            scheduleExpirationTimer()
            
            // Broadcast new ephemeral identifier for this epoch
            broadCastEphemeralIdentifier()
            
            // Save current epoch
            save(retry: 3, error: nil)
            if let jsonData = try? jsonEncoder.encode(epochAndSeed) {
                UserDefaults.standard.set(jsonData, forKey: ud_Epoch)
            }
        }
    }
    
    // MARK: - Public methods
    init() {
        // Fetch current Epoch if we had one
        if let jsonData = UserDefaults.standard.data(forKey: ud_Epoch) {
            epochAndSeed = try? JSONDecoder().decode(DP3TEpochAndSeed.self, from: jsonData)
        }
    }
    
    func start(epochChangeCallback: @escaping ((UUID) -> Void)) {
        LogHelper.debugLog("[DP3TService] Start")
        
        self.epochChangeCallback = epochChangeCallback
    }
    
    func stop() {
        epochChangeCallback = nil
        
        epochTimer?.invalidate()
        epochTimer = nil
        
        LogHelper.debugLog("[DP3TService] Stop")
    }
    
    func resetAll() {
        UserDefaults.standard.removeObject(forKey: ud_Epoch)
        if isStarted && epochChangeCallback != nil {
            checkEpochAndSeedExpiration()
        }
    }
    
    func generateHashedIdentityBase64(ephemeralIdentifier: UUID, epochIdentifier: Epoch) -> String {
        
        var rawEphemeralIdentifier = [UInt8](repeating: 0, count: 16)
        (ephemeralIdentifier as NSUUID).getBytes(&rawEphemeralIdentifier)
        
        var rawHashedIdentifier = [UInt8](repeating: 0, count: 32)
        EPH_hash_id(&rawEphemeralIdentifier, epochIdentifier, &rawHashedIdentifier)
        let hashedIdentifierData = Data(rawHashedIdentifier)
        
        return hashedIdentifierData.base64EncodedString()
    }
    
    func checkForRiskyInteractions(contacts: [ContactRecord], tracesetJsonData: Data) -> Date? {
        
        guard var cString = String(data: tracesetJsonData, encoding: .utf8)?.utf8CString else {
            LogHelper.debugLog("[DP3TService] Error: Could not convert jsonData to C-String!")
            return nil
        }
        
        guard let lastChar = cString[safe: cString.count-1], lastChar == 0 else {
            LogHelper.debugLog("[DP3TService] Error: Could not check for risky interactions, JSON not \0 terminated!")
            return nil
        }
        
        var cuckooFilter: OpaquePointer?
        guard CF_deserialize(&cString[0], &cuckooFilter) == 0 else {
            // Something went wrong, skip!
            return nil
        }
        
        // Check for risky interaction matches. Sort all the matches by date. Return the most recent date.
        let mostRecentMatch = contacts
            .compactMap { (contactRecord) -> Date? in
                if var hashedIdentity: HashedIdentity = contactRecord.rawHashedIdentity {
                    return CF_contains(cuckooFilter, &hashedIdentity) ? contactRecord.timestamp : nil
                }
                return nil
            }
            .sorted()
            .last
        
        CF_free(cuckooFilter)
        
        return mostRecentMatch
    }
    
    // MARK: - Private methods
    
    /// Check for nil and expiration, refreshes epoch/seed when needed
    @discardableResult private func checkEpochAndSeedExpiration() -> Bool {
        
        guard isStarted else { return false }
        
        // Check whether we need to refresh
        guard epochAndSeed == nil || epochAndSeed?.nextAfter.isInThePast() == true else {
            // If we accidentally fired just before expiration date,
            // schedule a new timer.
            scheduleExpirationTimer()
            LogHelper.debugLog("[DP3TService] Current \(epochAndSeed.debugDescription)")
            return false
        }

        let epochAndSeed = generateNewEpoch()
        
        // Debug logging
        LogHelper.debugLog("New \(epochAndSeed.debugDescription)")
       
        // Check new epoch
        guard epochAndSeed.nextAfter.isInThePast() == false && epochAndSeed.id != self.epochAndSeed?.id else {
            // This never happens :-D
            // So handle it, _when_ it does! #DefensiveProgramming
            // Wait one second and try getting another new Epoch
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) { [weak self] in
                self?.checkEpochAndSeedExpiration()
            }
            return false
        }
            
        // Save our new epoch
        self.epochAndSeed = epochAndSeed
        
        return true
    }
    
    private func generateNewEpoch() -> DP3TEpochAndSeed {
        // Generate new Epoch
        let epochInfo = EPOCH_INFO_from_timestamp(Int64(Date().timeIntervalSince1970))
        let nextEpochDate = Date(timeIntervalSince1970: TimeInterval(epochInfo.next_after))
        
        // Generate new Seed
        var rawSeed = [UInt8](repeating: 0, count: 32)
        EPH_generate_seed(&rawSeed)
        let seed = Data(rawSeed)
        
        // Create Swift friendly model
        return DP3TEpochAndSeed(
            id: epochInfo.id,
            seed: seed,
            nextAfter: nextEpochDate
        )
    }
    
    /// Broadcast an ephemeral identifier
    private func broadCastEphemeralIdentifier() {
        
        guard isStarted, let epochAndSeed = epochAndSeed else { return }
        
        // Get back the seed in raw [Uint8] form
        let rawSeed = Array(epochAndSeed.seed)
        
        // Get ephemeral identifier in raw form
        var rawEphemeralIdentifier = [UInt8](repeating: 0, count: 16)
        EPH_generate_id(rawSeed, &rawEphemeralIdentifier)
        
        epochChangeCallback?(NSUUID(uuidBytes: rawEphemeralIdentifier) as UUID)
    }
    
    private func scheduleExpirationTimer() {
        self.epochTimer?.invalidate()
        
        guard isStarted, let epochAndSeed = epochAndSeed else { return }

        let epochTimer = Timer(
            fire: epochAndSeed.nextAfter,
            interval: 0,
            repeats: false,
            block: { [weak self] _ in
                self?.checkEpochAndSeedExpiration()
            }
        )
        self.epochTimer = epochTimer
        RunLoop.main.add(epochTimer, forMode: .common)
    }
    
    private func save(retry tries: Int, error: DataError?) {
        // Save current epoch
        if tries > 0 {
            epochAndSeed?.saveToCoreData { result in
                switch result {
                case .success:
                    break
                case .failure(let error):
                    self.save(retry: tries - 1, error: error)
                }
            }
        } else {
            LogHelper.debugLog("[DP3TService] Failed to save seed and epoch to CoreData, error: \(error?.localizedDescription ?? "unknown error")")
        }
    }

}

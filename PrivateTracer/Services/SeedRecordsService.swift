//
//  SeedRecordsService.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 17/04/2020.
//

import Foundation

/// Lets you retrieve the saved seeds
protocol SeedRecordsService {
    
    /// Fetch the list of seeds.
    /// - Parameter completionBlock: Result with either a failure or the array of seed records
    func seedHistory(completionBlock: @escaping (Result<[SeedRecord], DataError>) -> Void)
    
    /// Delete old data, using setting for how old from PrivateTracerConfig
    /// - Parameter completionBlock: Result with either a failure or number of records that were deleted
    func removeOldData(completionBlock: @escaping (Result<Int, DataError>) -> Void)
    
    /// Delete all data
    /// - Parameter completionBlock: Result with either a failure or number of records that were deleted
    func removeAllData(completionBlock: @escaping (Result<Int, DataError>) -> Void)
}

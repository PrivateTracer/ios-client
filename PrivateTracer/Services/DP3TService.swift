//
//  DP3TService.swift
//  PrivateTracer
//
//  Created by Stijn Spijker.
//

import Foundation

/// Provides you with Epoch and seed information, uses UserDefaults to store the current
/// Automatically refreshes when the epoch expires.
protocol DP3TService {

    /// Start Epoch monitoring / saving
    /// - Parameter epochChangeCallback: Callback when the Epoch renews / changes, argument is the new Ephemeral UUID to broadcast
    func start(epochChangeCallback: @escaping ((UUID) -> Void))
    
    /// Stop Epoch monitoring / saving
    func stop()
    
    /// Reset the epoch, this deletes the stored epoch and seed in the NSUserDefaults
    /// Do this when deleting all the data
    func resetAll()
    
    /// The current Epoch with seed, refreshes automatically.
    var epochAndSeed: DP3TEpochAndSeed? { get }
    
    /// Generated a hashed identifier for a contact based on eph_id and current epoch
    /// - Parameters:
    ///   - ephemeralIdentifier: ephemeral ID the contact was broadcasting, matching the epoch
    ///   - epochIdentifier: epoch identifier of contact
    func generateHashedIdentityBase64(ephemeralIdentifier: UUID, epochIdentifier: Epoch) -> String
    
    /// Call for every traceset you download to check for risky interactions.
    /// - Parameters:
    ///   - contacts: Contacts we had in the past X days
    ///   - cuckooJsonData: Traceset JSON data
    /// - Returns: Nil if no risky interactions found, most recent date of risky interactions when found
    func checkForRiskyInteractions(contacts: [ContactRecord], tracesetJsonData: Data) -> Date?
}

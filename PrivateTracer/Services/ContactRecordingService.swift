//
//  ContactRecordingService.swift
//  PrivateTracer
//
//  Created by Dirk Groten on 19/04/2020.
//

import Foundation

/// Provides functionality to save and retrieve contacts.
/// A contact is a combination of the ephermeralID with the epochID at which it occured.
protocol ContactRecordingService {
    
    /// Save a contact event to the database.
    /// - Parameter withEphID: A `UUID` that was received from the contact
    /// - Parameter timestamp: The `Date` at which this contact was last seen
    /// - Parameter ephochID: The DP3T epoch ID
    func recordContact(withEphID: UUID, timestamp: Date, epochID: Epoch, completion: @escaping (Result<Void, DataError>) -> Void)
    
    /// Fetch all recorded contacts at once.
    /// Warning: This is still on the main thread, will be changed soon
    /// - Returns: An array of contacts
    func recordedContacts(completion: @escaping (Result<[ContactRecord], DataError>) -> Void)
    
    /// Delete old data, using setting for how old from PrivateTracerConfig
    func removeOldData(completion: @escaping (Result<Int, DataError>) -> Void)
    
    /// Delete all data
    func removeAllData(completion: @escaping (Result<Int, DataError>) -> Void)
}

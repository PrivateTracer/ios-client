# iOS Client

## Development process

We follow the [git-flow](https://nvie.com/posts/a-successful-git-branching-model/) branching model at the moment, as we're not ready to directly merge our features into the master branch. 

Please switch to the `develop` branch to see the latest Readme.md.

We version our app with semver in the following style: `major.minor.patch+buildnumber`, for example: `1.0.2+1337`.
Release branches are created this way, excluding the `+build`, for example: `release/1.0.2`.
We plan to make every new commit on the release branches trigger a build to TestFlight.
Merging the release branch (or correct tag) to `master` would still be a manual process. 

### Contributing

If you are contributing a larger feature, please create a feature branch `feature/name-of-feature` and when done, make a pull request into `develop`.

If you feel you are contributing a small feature or bug fix, just merge yourself into `develop` without pull request. We leave it up to you to decide if you'd like a review before merging or not. 

Once we have a proper CI and a first release of the app, anything merged into `develop` will get merged into `master` when a new release is made.

## Set-up local development service and Test APIs on Mac OS

1. `git clone https://gitlab.com/PrivateTracer/server.azure`
2. Follow the [server's README](https://gitlab.com/PrivateTracer/server.azure) in order to configure and start the server
3. Use a tool like Postman to test the API endpoints as explained in the README.

Hints for trying out the /tracer POST:
- Tracer object must have token of at least 9 alphanumeric characters. The current implementation just generates a random token, spec and library for this will come later.
- Example seed (in base64): PgNjCHMeCgXDX4ivBErmsMDSRYwENFLffGvr0yC/I4c=
- Seed already has validation for Base64.

## Updating the DP3T Lib

1. Follow the [README of the project dp3t-rust](https://gitlab.com/PrivateTracer/dp3t-rust), and check the prerequisites:
    * You have installed `rust`, `cargo`, and `cbindgen` 
    * You added to the toolchain both the target architectures: `aarch64-apple-ios` and `x86_64-apple-ios`

### Build and include the DP-3T library

2. cd into `ios-client/DP3T-Rust` and execute `./build.sh`
* Wait for the build completes, and the updated `libdp3t.h` and `libdp3t.a` will be copied int o the `DP3T-Rust` folder   

## Bluetooth communication

### What iOS is advertising

The device advertises a service with UUID `00001D1D-0000-1000-8000-00805F9B34FB`, stored in `PrivateTracerConfig.BluetoothServiceID`. The `ephemeralID` is broadcasted as the UUID of a service characteristic (`CBMutableCharacteristic`) with value `nil` for this service. 

### How to read the iOS ephemeral ID

Because the ephID isn't contained in the service advertisement, the scanning device must connect to the iOS BT broadcaster when it sees a service advertisement for the `BluetoothServiceID` of PrivateTracer.

When the connection is established, it needs to `discoverServices` with the `BluetoothServiceID` as UUID. 

When the service is discovered, it needs to `discoverCharacteristics` for that service, without specifying a UUID for which characteristic (passing `nil`), i.e. listing all characteristics of the service.

When the characteristics are discovered, there will be only one characteristic, the UUID of which is the `ephemeralID`. So there's no need to read the value of the characteristic

### How the iOS `CBCentralManager` records observations

The first time a PrivateTracer (PT) peripheral is discovered, the power level (`txLevel`) and RSSI (`rssi`) are cached, with as reference the peripheral identifier (`UUID`). They cannot be associated with an ephemeral ID yet, so we just keep them in cache.

After the connection, a new observation is made, this time with `ephID`, so the `ObservationManager` can now link this with the previous cached observation and store a "full observation"

During an epoch cycle, the BluetoothCentralService will see the same peripheral multiple times. The second time it sees a peripheral for which it already got a full observation, there's not need to make a connection anymore, since the `ephID` is known.

### What Android is advertising

An Android device advertises the `ephemeralID` directly in the service data payload, i.e. the data can retrieved in the advertisement data using the `CBAdvertisementDataServiceDataKey`.

## Functional and technical explanation of determining risky interactions

### How Observations are saved as Contacts

The Bluetooth scanner runs with the following algorithm:

- A scan is performed every 30 seconds (`PrivateTracerConfig.CentralScanInterval`)
- The scan duration is 10 seconds (`PrivateTracerConfig.CentralScanDuration`)
- During one epoch, there will be multiple scan, hence possibly multiple observations
- After each epoch, all observations are collected and changed into contacts, stored in CoreData as `ContactRecord`. 

The criteria for a set of observations with the same `ephID` to be stored as `ContactRecord` are based on two parameters:

- The minimum RSSI level required for an observation to be classified as risky observation, set in `PrivateTracerConfig.RiskyContactMinimumRssi`
- The minimum duration of a contact, set in `PrivateTracerConfig.RiskyContactMinimumDuration`: This cannot be longer than the duration of one epoch, current 5 minutes. After discarding all non-risky observations (too low RSSI), we check that the time interval between the latest observation and first observation is larger than this minimum duration. We also check the number of "risky" observations was equal to the number of scans performed during the minimum duration (duration / scan interval).

The contact recorded is a hash of `ephID` + `epochID`, which is sufficient to be used later when checking against the Cuckoo Filter of infected `ephID`s .

### Functional description of how tracesets work

In the short story below, person A will perform a new install of the app and meet person B. Person B turns out the be infected, and will get this confirmed by a doctor. Person A will then receive a "risky interactions" notification.

- Day 1 - Person A installs the app.
- Day 2 - Person A goes to the supermarket with bluetooth tracing on. Person B also goes to the supermarket with bluetooth tracing on, then stand next to each other in line for a while.
- Day 3 - Person B develops suspicious COVID-19 signs, and presses the "I Am Infected" button in the app. The app sends person B's epoch ID's and seeds to the server with a confirmation code for the doctor. Person B calls the doctor for an appointment.
- Day 4 - Person B goes to the doctor to get a COVID-19 diagnose. The doctor decides to do a test. Person B goes home.
- Day 5 - The doctor receives the test back, Person B is COVID-19 positive. The doctor confirms person B's confirmation code in the hospital system. The hospital system connects to PrivateTracer.org's backend and confirms the confirmation code. The backend marks the epoch ID's and seeds belonging to the confirmation code as infected and creates a new traceset.
- Day 6 - Person A opens up the app again to go to the supermarket. The app downloads the newest tracesets and finds matching traces with the cuckoofilter.

### Get tracesets from the server

The list of active tracesets can be fetched from `/config`. This returns a JSON dictionary of various parameters/lists that can be downloaded from the server.

The key `"TracingSets"` will give a list of tracing set IDs that are currently active.

The client should now:

1. Update its stored list of already downloaded IDs (empty the first time): remove any ID that isn't active anymore (not in the list fetched from the server) and compute the list of IDs it hasn't fetched yet.
2. Fetch all of the tracing sets by ID from `/tracingsets/{id}`. Note if there is nothing to match against (first time), it should not download the tracing sets.
3. Store all the successfully retrieved tracing set IDs persistently so it doesn't re-download them (the first time include all the ones that don't need to be downloaded). If an error occurs, those IDs aren't stored and therefore retried next time.
4. Match stored interactions with the downloaded tracing sets (see [next paragraph](#match-against-tracesets)).
5. Repeat every hour.

### Match against tracesets

For every traceset you download, you call `CF_deserialize_blob` with the base64-encoded data (`\0` terminated string!), this will give you a cuckoo filter that you can match against. Check the return value, it should be 0. (TBD: What if not 0? Stop fetching tracesets?)

Now you have the cuckoofilter, loop through your contacts (HashedIdentity) and call `CF_contains` repeatedly. Check the return value, if `true` the app has seen "risky interactions", and you need to warn the user.

The saved contact moments should also contain a timestamp. When warning the user to stay inside for X days, use the _last_ risky interaction the user had. So if traceset 1 matches on 4 days ago, but later you receive a traceset on day 8, the user needs to stay inside 4 extra days (X days staring from day 8).


#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define EPHID_HASH_LENGTH (256 / 8)

#define EPHID_ID_LENGTH 16

#define EPHID_SEED_LENGTH 32

/**
 * Length of a single epoch in minutes
 */
#define EPOCH_LENGTH 5

#define ID_TIME_ENTRY_LENGTH (EPHID_ID_LENGTH + 4)

typedef struct SimpleCuckooFilter SimpleCuckooFilter;

typedef uint8_t HashedIdentity[EPHID_HASH_LENGTH];

typedef int32_t Epoch;

typedef struct {
  /**
   * Current epoch id
   */
  Epoch id;
  /**
   * Request id again after this unix timestamp
   */
  int64_t next_after;
} EpochInfo;

/**
 * Adds an entry to the Cuckoo filter. Returns 0 on success, returns -1 when
 * there is not enough space in the filter.
 *
 * # Arguments
 *
 * `ptr` - Pointer to CuckooFilter struct.
 * `x` - 32 byte array of the hashed identity.
 */
int32_t CF_add(SimpleCuckooFilter *ptr, const HashedIdentity *x);

/**
 * Checks if the time entry is in the Cuckoo Filter. Returns true if it is, false if not.
 *
 * # Arguments
 *
 * `ptr` - Const pointer to the CuckooFilter struct.
 * `x` - 32 byte array of the hashed identity.
 */
bool CF_contains(const SimpleCuckooFilter *ptr, const HashedIdentity *x);

/**
 * Constructs a new CuckooFilter struct from a json string. Returns 0 on success
 * returns -1 on failure.
 *
 * The filter constructed using this function needs to be freed when you're doing
 * using it by calling CF_free().
 *
 * # Arguments
 *
 * `json_string` - C style string (0 terminated) containing the json.
 * `out` - Out pointer will contain pointer to the CuckooFilter on success.
 */
int32_t CF_deserialize(char *json_string, SimpleCuckooFilter **out);

/**
 * Frees the CuckooFilter
 *
 * # Arugments
 *
 * `ptr` - Pointer to CuckooFilter struct.
 */
void CF_free(SimpleCuckooFilter *ptr);

/**
 * Creates a new CuckooFilter struct.
 *
 * Struct needs to be freed by calling CF_free()
 *
 * # Examples
 * ```
 * use dp3t::cuckoo::*;
 *
 * let ptr = CF_new();
 * unsafe { CF_free(ptr) };
 * ```
 */
SimpleCuckooFilter *CF_new(void);

/**
 * Creates a new CuckooFilter struct with the specified size.
 *
 * Struct needs to be freed by calling CF_free()
 *
 * # Examples
 * ```
 * use dp3t::cuckoo::*;
 *
 * let ptr = CF_new_with_capacity(512);
 * unsafe { CF_free(ptr) };
 * ```
 */
SimpleCuckooFilter *CF_new_with_capacity(uintptr_t cap);

/**
 * Packs the CuckooFilter in a Json string. Returns 0 on success, returns -1 on failure.
 *
 * The string that is created in `out` needs to be free by calling CString_free().
 *
 * # Arguments
 *
 * `ptr` - Pointer to the CuckooFilter struct
 * `out` - Pointer where the C style json string will be located at if this function succeeds.
 */
int32_t CF_serialize(const SimpleCuckooFilter *ptr, char **out);

void CString_free(char *s);

/**
 * Generates an EPH id from a seed
 *
 * # Arguments
 *
 * `seed` - A pointer to a 32 byte unsigned byte array (generated by EPH_generate_seed)
 * `id` - A pointer to a 16 byte pre-allocated unsigned byte array.
 */
void EPH_generate_id(const unsigned char *seed, unsigned char *id);

/**
 * Generates an EPH seed
 *
 * # Arguments
 *
 * `seed` - A pointer to 32 byte pre-allocated unsigned byte array.
 */
void EPH_generate_seed(unsigned char *seed);

/**
 * Generates a Hashed Identity from an Ephemeral Id and an Epoch Id.
 *
 * # Arguments
 *
 * `id` - A pointer to a 16 byte unsigned byte array (generated by EPH_generate_id)
 * `epoch_id` - A 32 bit signed integer which represents the current Epoch id (obtained from EpochInfo.id)
 * `hashed_id` - A pointer to a 32 byte pre-allocated unsigned byte array.
 */
void EPH_hash_id(const unsigned char *id,
                 int epoch_id,
                 unsigned char *hashed_id);

/**
 * Constructs a new EpochInfo struct using the provided unix timestamp as base.
 *
 * You don't have to free this
 */
EpochInfo EPOCH_INFO_from_timestamp(int64_t ts);

/**
 * Constructs a new EpochInfo struct using the current unix timestamp as base.
 *
 * You don't have to free this
 */
EpochInfo EPOCH_INFO_new(void);

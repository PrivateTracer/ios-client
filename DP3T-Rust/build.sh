#!/bin/bash

# clone dp3t-rust in a temp dir, build the libraries and update them
# prerequisites: rust, cargo, and cbindgen installed
# 
build_d3pt_rust(){
    
    # temp directory 
    local tmp_dir=$(mktemp -d -t build-d3pt-rust-XXXXX)
    pushd "${tmp_dir}"

    #clone
    git clone https://gitlab.com/PrivateTracer/dp3t-rust
    cd dp3t-rust

    # Compile both the targets
    cargo build --release --target aarch64-apple-ios
    cargo build --release --target x86_64-apple-ios

    #Combine required architectures to form a universal library:
    lipo -create target/aarch64-apple-ios/release/libdp3t.a target/x86_64-apple-ios/release/libdp3t.a -output target/libdp3t.a

    popd

    # copy over lib and header
    cp "${tmp_dir}/dp3t-rust/target/libdp3t.h" ./
    cp "${tmp_dir}/dp3t-rust/target/libdp3t.a" ./

    # delete temp dir
    rm -rf "${tmp_dir}"
}

build_d3pt_rust
